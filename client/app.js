import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter as Router, routerMiddleware } from 'react-router-redux';
import { Route } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';

import reducers from './imports/reducers/index';

import App from './imports/app';



const history = createHistory();
const middleware = routerMiddleware(history);
const store = createStore(reducers, composeWithDevTools(applyMiddleware(middleware, thunk)));



Meteor.startup(() => {
  render(
    <Provider store={ store }>
      <Router history={ history }>
        <App/>
      </Router>
    </Provider>
  , document.getElementById('root'));
});
