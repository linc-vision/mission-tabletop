import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, withRouter } from 'react-router-dom';

import First from './components/first/index'
import Second from './components/second/index'
import Footer from './../notlogged/components/footer/index'
import Modal from './../notlogged/components/modal/index'



function mapStateToProps (state) {
  return {
    app: state.app,
    user: state.user,
    game: state.game
  }
}

function mapDispatchToProps (dispatch, props) {
  return {
    onStart: (data) => {
      dispatch({ type: 'START', payload: data })
    },
    onUpdateUser: (data) => {
      dispatch({ type: 'UPDATE_USER', payload: data })
    },
    onGetUser: (user) => {
      dispatch({ type: 'GET_USER', payload: user })
    }
  }
}



class Logged extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalType: false
    }
  }



  closeModal() {
    this.setState({
      modalType: false
    })
  }

  openModal(type) {
    this.setState({
      modalType: type
    })
  }



  render() {
    return(
      <div id='Logged' style={{ background: "linear-gradient(to bottom, #1F2638, #" + this.props.game.color + ", #1F2638)" }}>
        <div id='layout'>
          <First openModal={ this.openModal.bind(this) }/>
          <Second openModal={ this.openModal.bind(this) }/>
          <Footer openModal={ this.openModal.bind(this) }/>
        </div>
        <Route path='/:modal' render={ () => ( <Modal close={ this.closeModal.bind(this) } type={ this.state.modalType }/> ) }/>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Logged));
