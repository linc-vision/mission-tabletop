import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import $ from 'jquery.transit';
import copy from 'copy-to-clipboard';



function mapStateToProps (state) {
  return {
    game: state.game
  }
}



class Second extends Component {
  constructor(props) {
    super(props)
    this.state = {
      rulesLinkCopied: false
    }
    this.closeRulesSharing === this.closeRulesSharing.bind(this)
  }



  showTrivia() {
    $('#Logged #Second #trivia').transition({
      visibility: 'visible',
      top: "0",
      opacity: 1
    }, 200)
  }

  hideTrivia() {
    $('#Logged #Second #trivia').transition({
      visibility: 'hidden',
      top: "18px",
      opacity: 0
    }, 100)
  }

  shareRules() {
    $('#App').css({
      overflowY: 'hidden'
    })
    $('#Second #shareRules').fadeIn(200, () => {
      $('#Second #shareRules #layout').transition({
        top: "50%",
        opacity: 1,
        transform: "translate3d(-50%, -50%, 0) scale(1)"
      }, 200)
    })
  }

  copyRulesLink() {
    copy(window.location + 'rules/' + this.props.game.title.toLowerCase())
    this.setState({
      rulesLinkCopied: true
    }, () => {
      setTimeout(() => {
        this.closeRulesSharing()
      }, 2000)
    })
  }

  closeRulesSharing() {
    $('#Second #shareRules #layout').transition({
      top: "55%",
      opacity: 0,
      transform: "translate3d(-50%, -50%, 0) scale(0.6)"
    }, 200, () => {
      $('#Second #shareRules').fadeOut(200, () => {
        this.setState({
          rulesLinkCopied: false
        })
      })
    })
    $('#App').css({
      overflowY: 'auto'
    })
  }



  render() {
    return(
      <div id='Second'>
        <div id='layout'>
          <div id='title'><h1>Overview</h1></div>
          <div id='content'>
            <div id='left' onMouseOut={ this.hideTrivia.bind(this) }>
              <img src={ this.props.game.images.game_main } alt={ this.props.game.title } onMouseOver={ this.showTrivia.bind(this) }/>
              <p id='trivia'><strong>Trivia: </strong>{ this.props.game.first.trivia }</p>
            </div>
            <div id='right'>
              <h2>{ this.props.game.first.title }</h2>
              <p>{ this.props.game.first.text }</p>
              <Link to='/checkout'><button onClick={ this.props.openModal.bind(this, 'checkout') }>Get the game</button></Link>
            </div>
            <div id='trailer'>
              <div id='left'>
                <h2>{ this.props.game.second.title }</h2>
                <p>{ this.props.game.second.text }</p>
                <p id='ruleslink' onClick={ this.shareRules.bind(this) }>Click to copy the link to the game rules</p>
              </div>
              <div id='right'>
                <div id='video'>
                  <div id='youtube_vid'>
                    <iframe title="video" width="100%" height="100%" src={ this.props.game.second.video } frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen"  msallowfullscreen="msallowfullscreen"  oallowfullscreen="oallowfullscreen"  webkitallowfullscreen="webkitallowfullscreen"></iframe>
                    <div id='layer'></div>
                  </div>
                  <div id='layout'></div>
                </div>
              </div>
            </div>
            <div id='table'>
              <div id='left'>
                <div className='post' id='year'>
                  <h3>Year Published</h3>
                  <div id='picture'></div>
                  <h4>{ this.props.game.details.year }</h4>
                </div>
                <div className='post' id='designer'>
                  <h3>Designer</h3>
                  <div id='picture'></div>
                  <h4>{ this.props.game.details.designer }</h4>
                </div>
                <div className='post' id='categories'>
                  <h3>Categories</h3>
                  <div id='picture'></div>
                  <h4>{ this.props.game.details.categories }</h4>
                </div>
              </div>
              <div id='center'>
                <img src={ this.props.game.images.pic } alt={ this.props.game.title } />
              </div>
              <div id='right'>
                <div className='post' id='community'>
                  <div id='picture'></div>
                  <h3>Community</h3>
                  <h4>{ this.props.game.details.community }</h4>
                </div>
                <div className='post' id='playing_time'>
                  <div id='picture'></div>
                  <h3>Playing time</h3>
                  <h4>{ this.props.game.details.time }</h4>
                </div>
                <div className='post' id='age'>
                  <div id='picture'></div>
                  <h3>Age</h3>
                  <h4>{ this.props.game.details.age }</h4>
                </div>
              </div>
            </div>
            <div id='btn'>
            <Link to='/checkout'><button onClick={ this.props.openModal.bind(this, 'checkout') }>Get the game</button></Link>
              { /*<a onClick={ this.props.openModal.bind(this, 'pgames') }>See our prevoius Games</a>*/ }
            </div>
          </div>
        </div>
        <div id='shareRules'>
          <div id='layout'>
            <div id='header'>
              {(() => {
                if (!this.state.rulesLinkCopied) {
                  return <h3>Share</h3>
                }
              })()}
              <div id='close' onClick={ this.closeRulesSharing.bind(this) }></div>
            </div>
            <div id='content'>
              {(() => {
                if (!this.state.rulesLinkCopied) {
                  return [<p key='1'>Copy this link and share the rules of the game with your friends!</p>,
                  <div key='2' id='link' onClick={ this.copyRulesLink.bind(this) }>
                    <p>{ window.location + 'rules/' + this.props.game.title.toLowerCase() }</p>
                  </div>]
                } else {
                  return <p id='copied'>Copied to Clipboard!</p>
                }
              })()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(Second));
