import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import $ from 'jquery.transit';



function mapStateToProps (state) {
  return {
    game: state.game
  }
}



class First extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    $('#App').scroll(() => {
      if ($('#App').scrollTop() <= $('#First').height()) {
        $('#First > #bg').transition({
          transform: 'translateY(' + ($('#App').scrollTop() / 10) + 'px) scale(' + (1 - $('#App').scrollTop() / 1000) + ')'
        }, 0)
        $('#First > #wrapper').transition({
          transform: 'translateY(-' + ((1 + $('#App').scrollTop() / 10) + $('#First > #wrapper').height() / 2) + 'px) scale(' + (1 - $('#App').scrollTop() / 1600) + ')'
        }, 0)
      }
    })
  }



  scrollTo() {
    $('#App').animate({
      scrollTop: $('#First').height() + 80
    }, 600);
  }



  render() {
    return(
      <div id='First'>
        <div id='header'>
          <div id='left'>
            <img src='/img/icons/logo.png' alt="Mission Tabletop" />
            <h2>Mission tabletop</h2>
          </div>
          <div id='center'>
            <img src={ this.props.game.images.game_icon } alt={ this.props.game.title } />
          </div>
          <div id='right'>
            <Link to='/profile'><button onClick={ this.props.openModal.bind(this, 'profile') }>Profile</button></Link>
          </div>
        </div>
        <div id='wrapper'>
          <div id='name_g'>
            <img src={ this.props.game.images.game_title } alt={ this.props.game.title } />
          </div>
          <div id='content'>
            <p>{ this.props.game.subtitle }</p>
          </div>
        </div>
        <div id='footer'>
          <div id='scroll'>
            <div id='dots'></div>
            <div id='down' onClick={ this.scrollTo }></div>
          </div>
        </div>
        <div id='bg'></div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(First));
