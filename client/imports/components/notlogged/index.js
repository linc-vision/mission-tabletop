import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import First from './components/first/index'
import Second from './components/second/index'
import Footer from './components/footer/index'
import Modal from './components/modal/index'



class Notlogged extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalType: false
    }
  }



  closeModal() {
    this.setState({
      modalType: false
    })
  }

  openModal(type) {
    this.setState({
      modalType: type
    })
  }



  render() {
    return (
      <div id='Notlogged'>
        <div id='layout'>
          <First openModal={ this.openModal.bind(this) }/>
          <Second openModal={ this.openModal.bind(this) }/>
          <Footer openModal={ this.openModal.bind(this) }/>
        </div>
        <Route path='/:modal' render={ () => ( <Modal close={ this.closeModal.bind(this) } type={ this.state.modalType }/> ) }/>
      </div>
    );
  }
}

export default Notlogged
