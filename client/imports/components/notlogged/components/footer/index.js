import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery.transit'



class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date()
    }
  }



  scrollTo() {
    $('#App').animate({
      scrollTop: 0
    }, 600);
  }



  render() {
    return (
      <div id='Footer'>
        <div id='layout'>
          <div id='logo'>
            <img src='/img/icons/logo.png' alt='Mission Tabletop Logo' onClick={ this.scrollTo.bind(this) }/>
          </div>
          <p id='copyright'>{ "© Mission Tabletop, " + this.state.date.getFullYear() + ". All Rights Reserved" }</p>
          <div id='links'>
            <Link to='/privacy-policy' onClick={ this.props.openModal.bind(this, 'ppolicy') }>Privacy Policy</Link>
            <Link to='/return-policy' onClick={ this.props.openModal.bind(this, 'rpolicy') }>Return Policy</Link>
            <Link to='/contact-us' onClick={ this.props.openModal.bind(this, 'contact') }>Contact Us</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
