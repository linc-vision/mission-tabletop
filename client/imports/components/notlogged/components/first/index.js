import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery.transit';



class First extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    $('#App').scroll(() => {
      if ($('#App').scrollTop() <= $('#First').height()) {
        $('#First > #bg').transition({
          transform: 'translateY(' + ($('#App').scrollTop() / 10) + 'px) scale(' + (1 - $('#App').scrollTop() / 1000) + ')'
        }, 0)
        $('#First > #wrapper').transition({
          transform: 'translateY(-' + ((1 + $('#App').scrollTop() / 10) + $('#First > #wrapper').height() / 2) + 'px) scale(' + (1 - $('#App').scrollTop() / 1600) + ')'
        }, 0)
      }
    })
  }

  showTooltip(e) {
    $('body').append("<div id='tooltip'><p>Enter The Club</p></div>")
    $(document).mousemove((e) => {
      var currentMousePos = { x: -1, y: -1 };
      currentMousePos.x = e.pageX;
      currentMousePos.y = e.pageY;
      $('body').find('#tooltip').css({
        top: currentMousePos.y - 40,
        left: currentMousePos.x + 30
      })
    })
  }

  hideTooltip(e) {
    $('body').find('#tooltip').remove()
  }

  scrollTo() {
    $('#App').animate({
      scrollTop: $('#First').height()
    }, 600);
  }



  render() {
    return (
      <div id='First'>
        <div id='wrapper'>
          <div id='logo'>
            <img src='/img/icons/logo.png' alt="logo"/>
          </div>
          <div id='content'>
            <h2>Mission Tabletop</h2>
            <p>{ "The club for board gamers and newcomers alike, Mission Tabletop makes it easy for you to save a lot on a new tabletop experience - it’s a bargain with each game" }</p>
            <Link to='/signup'><div id='door' onClick={ this.props.openModal.bind(this, 'signup') } onMouseOver={ this.showTooltip.bind(this) } onMouseOut={ this.hideTooltip.bind(this) }></div></Link>
            <button onClick={ this.scrollTo.bind(this) }>Learn more</button>
          </div>
        </div>
        <div id='footer'>
          <div id='scroll'>
            <div id='dots'></div>
            <div id='down' onClick={ this.scrollTo.bind(this) }></div>
          </div>
        </div>
        <div id='bg'></div>
      </div>
    );
  }
}
export default First;
