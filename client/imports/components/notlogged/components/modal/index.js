import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import $ from 'jquery.transit';

import Sign from './components/sign/index'
import Profile from './components/profile/index'
import Checkout from './components/checkout/index'
import Game from './components/game/index'
import Rules from './components/rules/index'
// import Pgames from './components/pgames/index'
import Ppolicy from './components/ppolicy/index'
import Rpolicy from './components/rpolicy/index'
import Contact from './components/contact/index'



function mapStateToProps (state) {
  return {
    app: state.app
  }
}

function mapDispatchToProps (dispatch, props) {
  return {
    onChangePage: (data) => {
      dispatch({ type: 'CHANGE_PAGE', payload: data })
    }
  }
}


class Modal extends Component {

  componentDidMount() {
    $('#Modal').fadeIn(0, () => {
      $('#Modal > #layout').transition({
        right: "0",
        opacity: 1
      }, 200)
    })
    $('#App').css({ overflow: 'hidden' })
  }

  componentWillUnmount() {
    $('#Modal > #layout').transition({
      right: "-10%",
      opacity: 0
    }, 0)
    $('#Modal').fadeOut(100)
    $('#App').css({ overflowY: 'scroll' })
    this.props.onChangePage('main')
  }



  changePage(type) {
    this.props.onChangePage(type)
  }



  render() {
    return (
      <div id='Modal'>
        <div id='layout'>
          <Switch>
            {(() => {
              if (!this.props.app.user) {
                return [
                  <Route key="signup" path='/signup' render={ () => ( <Sign close={ this.props.close } type={ 'signup' } onChangePage={ this.changePage.bind(this) }/> ) }/>,
                  <Route key="login" path='/login' render={ () => ( <Sign close={ this.props.close } type={ 'login' } onChangePage={ this.changePage.bind(this) }/> ) }/>,
                  <Route exact key="game" path='/game' render={ () => ( <Game close={ this.props.close } type={ 'game' } onChangePage={ this.changePage.bind(this) }/> ) }/>
                ]
              } else {
                return [
                  <Route key="profile" path='/profile' render={ () => ( <Profile close={ this.props.close } type={ 'profile' } onChangePage={ this.changePage.bind(this) }/> ) }/>,
                  <Route key="checkout" path='/checkout' render={ () => ( <Checkout close={ this.props.close } type={ 'checkout' } onChangePage={ this.changePage.bind(this) }/> ) }/>
                ]
              }
            })()}
            { /* <Route path='/previous-games' render={ () => ( <Pgames close={ this.props.close } type={ 'pgames' } onChangePage={ this.changePage.bind(this) }/> ) }/> */ }
            <Route exact key="rules" path='/rules/dixit' render={ () => ( <Rules close={ this.props.close } type={ 'rules' } onChangePage={ this.changePage.bind(this) }/> ) }/>
            <Route path='/privacy-policy' render={ () => ( <Ppolicy close={ this.props.close } type={ 'ppolicy' } onChangePage={ this.changePage.bind(this) }/> ) }/>
            <Route path='/return-policy' render={ () => ( <Rpolicy close={ this.props.close } type={ 'rpolicy' } onChangePage={ this.changePage.bind(this) }/> ) }/>
            <Route path='/contact-us' render={ () => ( <Contact close={ this.props.close } type={ 'contact' } onChangePage={ this.changePage.bind(this) }/> ) }/>
            <Redirect path='/*' to='/'/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Modal));
