import React, { Component } from 'react';
import { Link } from 'react-router-dom';



class Rpolicy extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  componentDidMount() {
    this.props.onChangePage(this.props.type)
  }



  render() {
    return (
      <div id='Rpolicy'>
        <div id='layout'>
          <div id='header'>
            <Link to='/'><div id='close' onClick={ this.props.close }></div></Link>
            <h1>Return Policy</h1>
          </div>
          <div id='content'>
            <div id='layout'>
              <h4>Our policy lasts 30 days.</h4>
              <strong><i>If 30 days have gone by since your purchase, unfortunately we can’t offer you a refund or exchange.</i></strong>
              <p>To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.
              <br/>To complete your return, we require a receipt or proof of purchase.
              <br/>Please do not send your purchase back to the manufacturer.</p>
              <h3>Refunds (if applicable)</h3>
              <p>Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.
              <br/>If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.</p>
              <h3>Late or missing refunds (if applicable)</h3>
              <p>If you haven’t received a refund yet, first check your bank account again.
              <br/>Then contact your credit card company, it may take some time before your refund is officially posted.
              <br/>Next contact your bank. There is often some processing time before a refund is posted.
              <br/>If you’ve done all of this and you still have not received your refund yet, please contact us at <a href="mailto:support@missiontabletop.com" target="_top">support@missiontabletop.com</a>.</p>
              <h3>Exchanges (if applicable)</h3>
              <p>We only replace items if they are defective or damaged. If you need to exchange it for the same item, send us an email at <a href="mailto:support@missiontabletop.com" target="_top">support@missiontabletop.com</a>.</p>
              <h3>Shipping</h3>
              <p>To return your product, you should mail your product to the address provided.
              <br/>You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are non-refundable. If you receive a refund, the cost of return shipping will be deducted from your refund.
              <br/>Depending on where you live, the time it may take for your exchanged product to reach you, may vary.
              <br/>If you are shipping an item over $75, you should consider using a trackable shipping service or purchasing shipping insurance. We don’t guarantee that we will receive your returned item.</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Rpolicy;
