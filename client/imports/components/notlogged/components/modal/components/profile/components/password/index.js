import React, { Component } from 'react';
import $ from 'jquery.transit';



class Password extends Component {
  constructor(props) {
    super(props)
    this.state = {
      password: '',
      passwordValid: false
    }
    this.changePassword = this.changePassword.bind(this)
  }



  typePassword(e) {
    var target = e.target
    this.setState({
      password: target.value
    }, () => {
      if (this.state.password === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validPassword(e) {
    var valid = this.state.password.length > 7
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        passwordValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        passwordValid: true
      })
    }
  }

  changePassword() {
    if (this.state.passwordValid) {
      Meteor.call('setUserPassword', this.state.password, (error, result) => {
        if (error) {
          alert(error.reason)
        } else {
          this.success()
        }
      })
    }
  }

  success() {
    $('#close').click()
    $('#App').scrollTop(0)
  }



  render() {
    return (
      <div id='Password'>
        <div id='layout'>
          <div id='header'>
            <h1>Your Password</h1>
          </div>
          <div id='content'>
            <p id='email'>Email: <span id='state'>{ this.props.user.profile.email }</span></p>
            <p id='text'>Type a new  password to change it:</p>
            <div id='left'>
              <input className='empty' type="password" name="password" onChange={ this.typePassword.bind(this) } value={ this.state.password } onBlur={ this.validPassword.bind(this) }/>
              <span>new password</span>
            </div>
            <div id='right'>
              <button onClick={ this.changePassword }>Submit</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Password;
