import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Link, Route, Redirect, Switch } from 'react-router-dom';
import $ from 'jquery.transit';

import Phistory from './components/phistory/index';
// import Shipping from './components/shipping/index';
import Password from './components/password/index';
import Membership from './components/membership/index';



function mapStateToProps (state) {
  return {
    app: state.app,
    user: state.user
  }
}

function mapDispatchToProps (dispatch, props) {
  return {
    onGetUser: (user) => {
      dispatch({ type: 'GET_USER', payload: user })
    },
    onUpdateUser: (data) => {
      dispatch({ type: 'UPDATE_USER', payload: data })
    }
  }
}



class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentPage: 'phistory',
      mode: 'view',
      // updatedShipping: {
      //   name: this.props.user.profile.billing_address.name,
      //   address: this.props.user.profile.billing_address.address,
      //   state: this.props.user.profile.billing_address.state,
      //   city: this.props.user.profile.billing_address.city,
      //   zipcode: this.props.user.profile.billing_address.zipcode,
      //   country: this.props.user.profile.billing_address.country,
      //   phone: this.props.user.profile.billing_address.phone
      // }
    }
    this.selectMenuItem = this.selectMenuItem.bind(this)
    this.signOut = this.signOut.bind(this)
  }

  componentDidMount() {
    var route;
    switch (window.location.pathname) {
      case '/profile/purchase-history':
        route = 'phistory'
        break;
      // case '/profile/shipping-address':
      //   route = 'shipping'
      //   break;
      case '/profile/membership':
        route = 'membership'
        break;
      default:
        route = 'phistory'
        break;
    }
    this.setState({
      currentPage: route
    })
    this.props.onChangePage(this.props.type)
    $('#Profile #content #left').transition({
      left: 0,
      opacity: 1
    }, 400)
  }



  selectMenuItem(item) {
    $('#Profile #content #left').transition({
      left: "-10%",
      opacity: 0
    }, 100, () => {
      this.setState({
        currentPage: item
      }, () => {
        $('#Profile #content #left').transition({
          left: 0,
          opacity: 1
        }, 200)
      })
    })
  }

  signOut() {
    $('#loadingOver').fadeIn(100, () => {
      $('#loadingOver > #logo').fadeIn(200)
    })
    setTimeout(() => {
      Meteor.logout(() => {
        this.props.onGetUser(null)
        this.props.onUpdateUser()
      })
      $('#close').click()
      $('#App').scrollTop(0)
    }, 200)
    setTimeout(() => {
      $('#loadingOver > #logo').fadeOut(100, () => {
        $('#loadingOver').fadeOut()
      })
    }, 300)
  }

  toggleMode(shippingShouldUpdate = false) {
    var button = $('#Profile #footer #edit');
    if (this.state.mode === 'view') {
      this.setState({
        mode: 'edit'
      }, () => {
        $(button).addClass('submit')
      })
      if (this.state.currentPage === 'phistory' && this.props.user.profile.service === null) {
        this.selectMenuItem('password')
        this.props.history.push('/profile/password')
      } else if (this.state.currentPage === 'membership') {
        // this.selectMenuItem(shippingShouldUpdate ? 'shipping' : 'membership')
        this.props.history.push('/profile/membership')
      }
      // } else {
      //   this.selectMenuItem('shipping')
      //   this.props.history.push('/profile/shipping-address')
      // }
    } else if (this.state.mode === 'edit') {
      // this.updateProfile('shipping')
      if (this.state.currentPage === 'password') {
        this.selectMenuItem('phistory')
        this.props.history.push('/profile/purchase-history')
      }
      this.setState({
        mode: 'view'
      })
      $(button).removeClass('submit')
    }
  }

  // updateShipping(field, valid, data) {
  //   if (field === 'name') {
  //     this.setState({
  //       updatedShipping: {
  //         ...this.state.updatedShipping,
  //         name: data
  //       }
  //     })
  //   } else if (field === 'address') {
  //     this.setState({
  //       updatedShipping: {
  //         ...this.state.updatedShipping,
  //         address: data
  //       }
  //     })
  //   } else if (field === 'state') {
  //     this.setState({
  //       updatedShipping: {
  //         ...this.state.updatedShipping,
  //         state: data
  //       }
  //     })
  //   } else if (field === 'city') {
  //     this.setState({
  //       updatedShipping: {
  //         ...this.state.updatedShipping,
  //         city: data
  //       }
  //     })
  //   } else if (field === 'zipcode') {
  //     this.setState({
  //       updatedShipping: {
  //         ...this.state.updatedShipping,
  //         zipcode: data
  //       }
  //     })
  //   } else if (field === 'phone') {
  //     this.setState({
  //       updatedShipping: {
  //         ...this.state.updatedShipping,
  //         phone: data
  //       }
  //     })
  //   }
  // }

  // updateProfile(type) {
  //   if (type === 'shipping') {
  //     var data = this.state.updatedShipping;
  //     var userData = {
  //       name: data.name,
  //       address: data.address,
  //       state: data.state,
  //       country: 'USA',
  //       city: data.city,
  //       zipcode: data.zipcode,
  //       phone: data.phone
  //     }
  //     Meteor.call('updateUserProfile', userData, (error) => {
  //       if (error) {
  //         console.error(error.reason)
  //       } else {
  //         this.props.onGetUser(Meteor.user());
  //         this.setState({
  //           updatedShipping: {
  //             name: this.props.user.profile.billing_address.name,
  //             address: this.props.user.profile.billing_address.address,
  //             state: this.props.user.profile.billing_address.state,
  //             city: this.props.user.profile.billing_address.city,
  //             zipcode: this.props.user.profile.billing_address.zipcode,
  //             country: this.props.user.profile.billing_address.country,
  //             phone: this.props.user.profile.billing_address.phone
  //           }
  //         })
  //       }
  //     })
  //   }
  // }



  render() {
    return (
      <div id='Profile'>
        {(() => {
          if (!$.isEmptyObject(this.props.user)) {
            return <div id='layout'>
              <div id='header'>
                <Link to='/'><div id='close' onClick={ this.props.close }></div></Link>
                <h1>{ this.props.user.profile.name }</h1>
              </div>
              <div id='content'>
                <div id='left'>
                  <Switch>
                    <Route path='/profile/purchase-history' render={ () => ( <Phistory user={ this.props.user }/> ) }/>
                    {/* <Route path='/profile/shipping-address' render={ () => ( <Shipping user={ this.props.user } onSetMode={ this.toggleMode.bind(this) } mode={ this.state.mode } updateShipping={ this.updateShipping.bind(this) }/> ) }/> */}
                    {(() => {
                      if (this.state.mode === 'edit' && this.props.user.profile.service === null) {
                        return <Route path='/profile/password' render={ () => ( <Password user={ this.props.user }/> ) }/>
                      }
                    })()}
                    <Route path='/profile/membership' render={ () => ( <Membership user={ this.props.user } onSetMode={ this.toggleMode.bind(this) } mode={ this.state.mode }/> ) }/>
                    <Redirect from='/profile' to='/profile/purchase-history'/>
                  </Switch>
                </div>
                <div id='right'>
                  <div id='list'>
                    {(() => {
                      if (this.state.mode === 'view') {
                        return <Link to='/profile/purchase-history'><p id='phistory' className={ this.state.currentPage === 'phistory' ? 'active' : '' } onClick={ this.selectMenuItem.bind(this, 'phistory') }>Purchase History</p></Link>
                      }
                    })()}
                    {(() => {
                      if (this.state.mode === 'edit' && this.props.user.profile.service === null) {
                        return <Link to='/profile/password'><p id='password' className={ this.state.currentPage === 'password' ? 'active' : '' } onClick={ this.selectMenuItem.bind(this, 'password') }>Password</p></Link>
                      }
                    })()}
                    {/* <Link to='/profile/shipping-address'><p id='shipping' className={ this.state.currentPage === 'shipping' ? 'active' : '' } onClick={ this.selectMenuItem.bind(this, 'shipping') }>Shipping Address</p></Link> */}
                    {(() => {
                      if (this.state.mode === 'edit') {
                        return <Link to='/profile/membership'><p id='membership' className={ this.state.currentPage === 'membership' ? 'active' : '' } onClick={ this.selectMenuItem.bind(this, 'membership') }>Your Membership</p></Link>
                      } else if (this.state.mode === 'view') {
                        return <Link to='/profile/membership'><p id='membership' className={ this.state.currentPage === 'membership' ? 'active' : '' } onClick={ this.selectMenuItem.bind(this, 'membership') }>Your Membership</p></Link>
                      }
                    })()}
                  </div>
                </div>
              </div>
              <div id='footer'>
                <p onClick={ this.signOut }>Log out</p>
                <div id='edit' onClick={ this.toggleMode.bind(this, false) }></div>
              </div>
            </div>
          }
        })()}
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Profile));
