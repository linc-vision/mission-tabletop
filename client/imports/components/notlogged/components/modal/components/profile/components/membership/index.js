import { withTracker } from 'meteor/react-meteor-data';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';



function mapStateToProps (state) {
  return {
    user: state.user
  }
}

function mapDispatchToProps (dispatch, props) {
  return {
    onGetUser: (user) => {
      dispatch({ type: 'GET_USER', payload: user })
    }
  }
}



class Membership extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 'become',
      selectedType: '',
      subpage: '',
      type: 'visa',
      number: '',
      numberValid: false,
      expMonth: '',
      expMonthValid: false,
      expYear: '',
      expYearValid: false,
      cvv2: '',
      cvv2Valid: false,
      firstName: '',
      firstNameValid: false,
      lastName: '',
      lastNameValid: false
    }
    this.updateShipping = this.updateShipping.bind(this)
  }



  selectMembership() {
    this.setState({
      page: 'select'
    })
  }

  enterCard(type) {
    this.setState({
      selectedType: type,
      subpage: 'credit-card'
    })
  }

  cancelCard() {
    this.setState({
      selectedType: '',
      subpage: ''
    })
  }

  setType(e) {
    var target = e.target;
    this.setState({
      type: target.id
    })
  }

  typeNumber(e) {
    var target = e.target;
    this.setState({
      number: target.value.replace(/[^0-9]/, "")
    }, () => {
      if (this.state.number === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validNumber(e) {
    var valid = this.state.number.length === 16
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        numberValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        numberValid: true
      })
    }
  }

  typeExpMonth(e) {
    var target = e.target;
    this.setState({
      expMonth: target.value.replace(/[^0-9]/, "")
    }, () => {
      if (this.state.expMonth === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validExpMonth(e) {
    var valid = parseInt(this.state.expMonth, 10) <= 12 && parseInt(this.state.expMonth, 10) !== 0
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        expMonthValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        expMonthValid: true
      })
    }
  }

  typeExpYear(e) {
    var target = e.target;
    this.setState({
      expYear: target.value.replace(/[^0-9]/, "")
    }, () => {
      if (this.state.expYear === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validExpYear(e) {
    var number = parseInt(this.state.expYear, 10) < 2000 ? 2000 + parseInt(this.state.expYear, 10) : parseInt(this.state.expYear, 10);
    var valid = number > (new Date().getFullYear() - 1)
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        expYearValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        expYearValid: true
      })
    }
  }

  typeCvv2(e) {
    var target = e.target;
    this.setState({
      cvv2: target.value.replace(/[^0-9]/, "")
    }, () => {
      if (this.state.cvv2 === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validCvv2(e) {
    var valid = this.state.cvv2.length > 1 && this.state.cvv2.length < 6
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        cvv2Valid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        cvv2Valid: true
      })
    }
  }

  typeFirstName(e) {
    var target = e.target;
    this.setState({
      firstName: target.value.replace(/[^a-zA-Z- ]/, "")
    }, () => {
      if (this.state.firstName === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validFirstName(e) {
    var valid = this.state.firstName.length >= 2
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        firstNameValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        firstNameValid: true
      })
    }
  }

  typeLastName(e) {
    var target = e.target;
    this.setState({
      lastName: target.value.replace(/[^a-zA-Z- ]/, "")
    }, () => {
      if (this.state.lastName === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validLastName(e) {
    var valid = this.state.lastName.length >= 2
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        lastNameValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        lastNameValid: true
      })
    }
  }

  buyMembership(period) {
    // if (this.state.numberValid && this.state.expMonthValid && this.state.expYearValid && this.state.cvv2Valid && this.state.firstNameValid && this.state.lastNameValid) {
      // var data = {
      //   selectedType: this.state.selectedType,
      //   card_data: {
      //     type: this.state.type,
      //     number: this.state.number,
      //     expMonth: this.state.expMonth,
      //     expYear: this.state.expYear,
      //     cvv2: this.state.cvv2,
      //     firstName: this.state.firstName,
      //     lastName: this.state.lastName
      //   }
      // }
      // Meteor.call('selectBillingPlan', data, (error, res) => {
      //   if (error) {
      //     alert("Something went wrong. Try again later")
      //   } else {
      //     if (res === 'INVALID_DATA') {
      //       alert("Something went wrong. Try again later")
      //     } else if (res === 'ERROR') {
      //       alert('ERROR')
      //     } else {
      //       this.props.onGetUser(Meteor.user())
      //     }
      //   }
      // })
    // }
    Meteor.call('selectBillingPlan', period, (error, res) => {
      if (error) {
        alert("Something went wrong. Try again later")
      } else {
        window.location.href = res
        // console.log(res);
      }
    })
  }

  cancelMembership() {
    var result = confirm('Are you sure you want to cancel the subscription?')
    if (result) {
      Meteor.call('cancelMembership', (error, res) => {
        if (error) {
          alert("Something went wrong. Try again later")
        } else {
          if (res === 'ERROR') {
            alert("Something went wrong. Try again later [ Server ]")
          } else if (res === 'SUCCESS') {
            this.props.onGetUser(Meteor.user())
            this.props.history.push('/')
          }
        }
      })
    }
  }

  updateShipping() {
    if (this.props.mode === 'view') {
      this.props.onSetMode(true)
    }
  }



  render() {
    return (
      <div id='Membership'>
        <div id='layout'>
          <div id='header'>
            <h1>Your Membership</h1>
          </div>
          <div id='content'>
            {(() => {
              if (!this.props.user.profile.member) {
                if (this.state.page === 'become') {
                  return <div id='notmember'>
                    <p>You are not a Member</p>
                    <button id='bemember' onClick={ this.selectMembership.bind(this) }>Become a Member now!</button>
                  </div>
                } else if (this.state.page === 'select') {
                  if (this.state.subpage === '') {
                    return <div id='select-membership'>
                      <h3 id='select'>Select a plan:</h3>
                      <div className='plan'>
                        <h4>Monthly</h4>
                        <p><strong>$1</strong> per <strong>Month</strong></p>
                        <button id='select' onClick={ this.buyMembership.bind(this, 'monthly') }>Select</button>
                      </div>
                      <div className='plan'>
                        <h4>Annual</h4>
                        <p><strong>$10</strong> per <strong>Year</strong></p>
                        <button id='select' onClick={ this.buyMembership.bind(this, 'annual') }>Select</button>
                      </div>
                    </div>
                  } else {
                    return <div id='enter-credit-card'>
                      <h4 id='select'>Enter your credit card:</h4>
                      <div id='card'>
                        <div id='type'>
                          <span>Type</span>
                          <div className={ this.state.type === 'visa' ? 'icon active' : 'icon' } id='visa' onClick={ this.setType.bind(this) }></div>
                          <div className={ this.state.type === 'mastercard' ? 'icon active' : 'icon' } id='mastercard' onClick={ this.setType.bind(this) }></div>
                          <div className={ this.state.type === 'amex' ? 'icon active' : 'icon' } id='amex' onClick={ this.setType.bind(this) }></div>
                          <div className={ this.state.type === 'discover' ? 'icon active' : 'icon' } id='discover' onClick={ this.setType.bind(this) }></div>
                          <div className={ this.state.type === 'maestro' ? 'icon active' : 'icon' } id='maestro' onClick={ this.setType.bind(this) }></div>
                        </div>
                        <div id='number'>
                          <input type='text' name='number' className={ this.state.number !== '' ? '' : 'empty' } value={ this.state.number } onChange={ this.typeNumber.bind(this) } onBlur={ this.validNumber.bind(this) }/>
                          <span>Number</span>
                        </div>
                        <div id='cvv2'>
                          <input type='text' name='cvv2' className={ this.state.cvv2 !== '' ? '' : 'empty' } value={ this.state.cvv2 } onChange={ this.typeCvv2.bind(this) } onBlur={ this.validCvv2.bind(this) }/>
                          <span>CVV2</span>
                        </div>
                        <div id='expMonth'>
                          <input type='text' name='expire_month' className={ this.state.expMonth !== '' ? '' : 'empty' } value={ this.state.expMonth } onChange={ this.typeExpMonth.bind(this) } onBlur={ this.validExpMonth.bind(this) }/>
                          <span>Exp. Month</span>
                        </div>
                        <div id='expYear'>
                          <input type='text' name='expire_month' className={ this.state.expYear !== '' ? '' : 'empty' } value={ this.state.expYear } onChange={ this.typeExpYear.bind(this) } onBlur={ this.validExpYear.bind(this) }/>
                          <span>Exp. Year</span>
                        </div>
                        <div id='firstName'>
                          <input type='text' name='first_name' className={ this.state.firstName !== '' ? '' : 'empty' } value={ this.state.firstName } onChange={ this.typeFirstName.bind(this) } onBlur={ this.validFirstName.bind(this) }/>
                          <span>First Name</span>
                        </div>
                        <div id='lastName'>
                          <input type='text' name='last_name' className={ this.state.lastName !== '' ? '' : 'empty' } value={ this.state.lastName } onChange={ this.typeLastName.bind(this) } onBlur={ this.validLastName.bind(this) }/>
                          <span>Last Name</span>
                        </div>
                      </div>
                      <p id='cancel' onClick={ this.cancelCard.bind(this) }>Cancel</p>
                      <button id='enter' onClick={ this.buyMembership.bind(this) }>Enter</button>
                    </div>
                  }
                }
              } else {
                return <div id='member'>
                  <h4>You have an active subscription - <strong>{ this.props.user.profile.membership_agreement.period }</strong></h4>
                  <p id='next-payment'>{ "Next payment date: " + new Date(this.props.user.profile.membership_agreement.NEXTBILLINGDATE).toLocaleDateString() }</p>
                  {(() => {
                    if (this.props.mode === 'edit') {
                      return <p id='cancel' onClick={ this.cancelMembership.bind(this) }>Cancel Your Subscription</p>
                    }
                  })()}
                </div>
              }
            })()}
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Membership));
