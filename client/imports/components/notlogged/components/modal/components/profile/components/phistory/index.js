import React, { Component } from 'react';



class Phistory extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }



  render() {
    return (
      <div id='Phistory'>
        <div id='layout'>
          <div id='header'>
            <h1>Your Purchase History</h1>
          </div>
          <div id='content'>
            <div id='list'>
              {(() => {
                if (this.props.user.profile.purchase_history.length > 0) {
                  return this.props.user.profile.purchase_history.map((item) => {
                    return <div key={ item.id } className='item'>
                      <div id='left'>
                        <img src={ item.icon } alt={ item.name } />
                        <div id='info'>
                          <p id='name'>{ item.name }</p>
                          <p id='date'>{ item.date.getDate() + '/' + (item.date.getMonth() + 1) + '/' + item.date.getFullYear() }</p>
                        </div>
                      </div>
                      <div id='right'>
                        <h2>{ "$" + item.amount }</h2>
                      </div>
                    </div>
                  })
                } else {
                  return <p id='empty'>No Purchases Yet</p>
                }
              })()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Phistory;
