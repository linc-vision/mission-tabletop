import React, { Component } from 'react';
import $ from 'jquery.transit';


class Shipping extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: this.props.user.profile.billing_address.name,
      nameValid: false,
      address: this.props.user.profile.billing_address.address,
      addressValid: false,
      state: this.props.user.profile.billing_address.state,
      stateValid: false,
      city: this.props.user.profile.billing_address.city,
      cityValid: false,
      zipcode: this.props.user.profile.billing_address.zipcode,
      zipcodeValid: false,
      country: this.props.user.profile.billing_address.country,
      countryValid: true,
      phone: this.props.user.profile.billing_address.phone,
      phoneValid: false,
      error: 'Error'
    }
  }



  typeName(e) {
    var target = e.target;
    this.setState({
      name: target.value.replace(/[^a-zA-Z ]/, "")
    }, () => {
      if (this.state.name === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validName(e) {
    var valid = this.state.name.length > 6
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        nameValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        nameValid: true
      })
    }
    if (this.state.name !== this.props.user.profile.billing_address.name) {
      this.props.updateShipping('name', valid, this.state.name)
    }
  }


  typeAddress(e) {
    var target = e.target;
    this.setState({
      address: target.value
    }, () => {
      if (this.state.address === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validAddress(e) {
    var valid = this.state.address.length > 8
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        addressValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        addressValid: true
      })
    }
    if (this.state.address !== this.props.user.profile.billing_address.address) {
      this.props.updateShipping('address', valid, this.state.address)
    }
  }


  typeState(e) {
    var target = e.target;
    this.setState({
      state: target.value.replace(/[^a-zA-Z -]/, "")
    }, () => {
      if (this.state.state === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validState(e) {
    var valid = this.state.state.length > 2
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        stateValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        stateValid: true
      })
    }
    if (this.state.state !== this.props.user.profile.billing_address.state) {
      this.props.updateShipping('state', valid, this.state.state)
    }
  }


  typeCity(e) {
    var target = e.target;
    this.setState({
      city: target.value.replace(/[^a-zA-Z -]/, "")
    }, () => {
      if (this.state.city === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validCity(e) {
    var valid = this.state.city.length > 2
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        cityValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        cityValid: true
      })
    }
    if (this.state.name !== this.props.user.profile.billing_address.name) {
      this.props.updateShipping('name', valid, this.state.name)
    }
    if (this.state.city !== this.props.user.profile.billing_address.city) {
      this.props.updateShipping('city', valid, this.state.city)
    }
  }


  typeZipcode(e) {
    var target = e.target;
    this.setState({
      zipcode: target.value.replace(/[^0-9]/, "")
    }, () => {
      if (this.state.zipcode === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }


  validZipcode(e) {
    var valid = this.state.zipcode.length <= 6 && this.state.zipcode.length >= 5
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        zipcodeValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        zipcodeValid: true
      })
    }
    if (this.state.zipcode !== this.props.user.profile.billing_address.zipcode) {
      this.props.updateShipping('zipcode', valid, this.state.zipcode)
    }
  }


  typePhone(e) {
    var target = e.target;
    this.setState({
      phone: target.value.replace(/[^0-9()+ -]/, "")
    }, () => {
      if (this.state.phone === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validPhone(e) {
    var valid = this.state.phone.length <= 17 && this.state.phone.length > 11
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        phoneValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        phoneValid: true
      })
    }
    if (this.state.phone !== this.props.user.profile.billing_address.phone) {
      this.props.updateShipping('phone', valid, this.state.phone)
    }
  }



  render() {
    return (
      <div id='Shipping'>
        <div id='layout'>
          <div id='header'>
            <h1>Your Shipping Address</h1>
          </div>
          {(() => {
            if (this.props.mode === 'view') {
              return <div id='content' className='view'>
                <p id='name'>{ this.props.user.profile.billing_address.name }</p>
                {(() => {
                  if (this.props.user.profile.billing_address.name !== '' && this.props.user.profile.billing_address.state !== '' && this.props.user.profile.billing_address.city !== '' && this.props.user.profile.billing_address.zipcode !== '' && this.props.user.profile.billing_address.phone !== '' ) {
                    return [ <p key='address'>{ this.props.user.profile.billing_address.address !== '' ? this.props.user.profile.billing_address.address + ',' : '' }</p>,
                    <p key='state'>{ this.props.user.profile.billing_address.state + ', ' + this.props.user.profile.billing_address.city + ', ' + this.props.user.profile.billing_address.zipcode + ',' }</p>,
                    <p key='country'>{ this.props.user.profile.billing_address.country !== '' ? this.props.user.profile.billing_address.country : '' }</p>,
                    <p key='phone' id='phone'>{ this.props.user.profile.billing_address.phone !== '' ? this.props.user.profile.billing_address.phone : '' }</p> ]
                  } else {
                    return [ <br key='0'/>,
                    <p key='1' id='shouldupdate'>You must fill in all the fields. </p>,
                    <p key='2' onClick={ this.props.onSetMode }>Click here to update</p> ]
                  }
                })()}
              </div>
            } else if (this.props.mode === 'edit') {
              return <div id='content' className='edit'>
                <div id='name'>
                  <input type='text' name='name' className={ this.state.name !== '' ? '' : 'empty' } value={ this.state.name } onChange={ this.typeName.bind(this) } onBlur={ this.validName.bind(this) }/>
                  <span>Name</span>
                </div>
                <div id='address'>
                  <input type='text' name='address' className={ this.state.address !== '' ? '' : 'empty' } value={ this.state.address } onChange={ this.typeAddress.bind(this) } onBlur={ this.validAddress.bind(this) }/>
                  <span>Address</span>
                </div>
                <div id='state'>
                  <input type='text' name='state' className={ this.state.state !== '' ? '' : 'empty' } value={ this.state.state } onChange={ this.typeState.bind(this) } onBlur={ this.validState.bind(this) }/>
                  <span>State</span>
                </div>
                <div id='city'>
                  <input type='text' name='city' className={ this.state.city !== '' ? '' : 'empty' } value={ this.state.city } onChange={ this.typeCity.bind(this) } onBlur={ this.validCity.bind(this) }/>
                  <span>City</span>
                </div>
                <div id='zipcode'>
                  <input type='text' name='zipcode' className={ this.state.zipcode !== '' ? '' : 'empty' } value={ this.state.zipcode } onChange={ this.typeZipcode.bind(this) } onBlur={ this.validZipcode.bind(this) }/>
                  <span>Zipcode</span>
                </div>
                <div id='country'>
                  <input type='text' name='country' className={ this.state.country !== '' ? '' : 'empty' } disabled={ true } defaultValue={ this.state.country }/>
                  <span>Country</span>
                </div>
                <div id='phone'>
                  <input type='text' name='phone' className={ this.state.phone !== '' ? '' : 'empty' } value={ this.state.phone } onChange={ this.typePhone.bind(this) } onBlur={ this.validPhone.bind(this) }/>
                  <span>Phone</span>
                </div>
              </div>
            }
          })()}
        </div>
      </div>
    );
  }
}

export default Shipping;
