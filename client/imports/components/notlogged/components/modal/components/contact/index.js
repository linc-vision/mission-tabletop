import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import $ from 'jquery.transit';



function mapStateToProps (state) {
  return {
    user: state.user
  }
}



class Contact extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentUser: false,
      email: '',
      emailValid: false,
      name: '',
      nameValid: false,
      message: '',
      messageValid: false
    }
    this.enter = this.enter.bind(this)
    this.send = this.send.bind(this)
  }

  componentDidMount() {
    document.addEventListener('keydown', this.enter);
    if (this.props.user !== undefined) {
      this.setState({
        currentUser: true
      })
    }
    setTimeout(() => {
      if (this.state.currentUser) {
        if (this.props.user.profile.email === undefined) {
          $('#App #Modal #Contact input').eq(1).removeClass('empty')
          this.setState({
            name: this.props.user.profile.name,
            nameValid: true
          })
          $('#App #Modal #Contact input').eq(0).focus()
        } else {
          $('#App #Modal #Contact input').eq(0).removeClass('empty')
          $('#App #Modal #Contact input').eq(1).removeClass('empty')
          this.setState({
            email: this.props.user.profile.email,
            emailValid: true,
            name: this.props.user.profile.name,
            nameValid: true
          })
          $('#App #Modal #Contact textarea').eq(0).focus()
        }
      } else {
        $('#App #Modal #Contact input').eq(0).focus()
      }
    }, 1200)
    this.props.onChangePage(this.props.type)
  }




  typeEmail(e) {
    var target = e.target;
    this.setState({
      email: target.value.toLowerCase()
    }, () => {
      if (this.state.email === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validEmail(e) {
    var validTest = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    var valid = validTest.test(this.state.email)
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        emailValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        emailValid: true
      })
    }
  }

  typeName(e) {
    var target = e.target
    this.setState({
      name: target.value.replace(/[^a-zA-z]+/g, '')
    }, () => {
      if (this.state.name === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validName(e) {
    var valid = this.state.name.length > 1
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        nameValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        nameValid: true
      })
    }
  }

  typeMessage(e) {
    var target = e.target
    this.setState({
      message: target.value
    }, () => {
      if (this.state.message === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validMessage(e) {
    var valid = this.state.message.length > 2
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        messageValid: false
      })
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        messageValid: true
      })
    }
  }

  enter(e) {
    if (e.which === 13) {
      this.send()
    }
  }

  send() {
    if (!this.state.emailValid || !this.state.nameValid || !this.state.messageValid) {
      alert("Fields isn't valid")
    } else {
      var data;
      data = {
        email: this.state.email,
        name: this.state.name,
        message: this.state.message
      }
      Meteor.call('contactUs', data, (error, res) => {
        if (error) {
          alert('Something went wrong. Try again later.')
        } else {
          if (res === 'SUCCESS') {
            $('#Contact #close').click()
          }
        }
      })
    }
  }



  render() {
    return (
      <div id='Contact'>
        <div id='layout'>
          <div id='header'>
            <Link to='/'><div id='close' onClick={ this.props.close }></div></Link>
            <h1>Contact Us</h1>
          </div>
          <div id='content'>
            <div id='left'>
              <div>
                <input className='empty' type="email" name="email" required onChange={ this.typeEmail.bind(this) } value={ this.state.email } onBlur={ this.validEmail.bind(this) }/>
                <span>Your email</span>
              </div>
              <div>
                <input className='empty' name="name" required onChange={ this.typeName.bind(this) } value={ this.state.name } onBlur={ this.validName.bind(this) }/>
                <span>Your name</span>
              </div>
              <div>
                <textarea className='empty' rows='6' required onChange={ this.typeMessage.bind(this) } value={ this.state.message } onBlur={ this.validMessage.bind(this) }/>
                <span>Message</span>
              </div>
            </div>
            <div id='right' onClick={ this.send }></div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(Contact));
