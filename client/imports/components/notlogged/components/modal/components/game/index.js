import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';



function mapStateToProps (state) {
  return {
    game: state.game
  }
}



class Game extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }

  componentDidMount() {
    this.props.onChangePage(this.props.type)
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      gameTitle: newProps.gameTitle
    })
  }



  render() {
    return (
      <div id='Game'>
        <div id='layout'>
          <div id='header'>
            <Link to='/'><div id='close' onClick={ this.props.close }></div></Link>
            <div id='nameg'><img src={ this.props.game.images.game_title } alt="gt" /></div>
          </div>
          <div id='content'>
            <div id='left'>
              <h1>{ this.props.game.first.title }</h1>
              <div id='text'>
                <p>{ this.props.game.first.text }</p>
              </div>
            </div>
            <div id='right'>
              <Link to='/signup'><button>Join to get</button></Link>
              <div id='main'>
                <img src={ this.props.game.images.game_main } alt="gm" />
              </div>
              <div id='merit'>
                <div id='left'>
                  <div className='post' id='year'>
                    <h3>Year Published</h3>
                    <div id='picture'></div>
                    <h4>{ this.props.game.details.year }</h4>
                  </div>
                  <div className='post' id='designer'>
                    <h3>Designer</h3>
                    <div id='picture'></div>
                    <h4>{ this.props.game.details.designer }</h4>
                  </div>
                  <div className='post' id='categories'>
                    <h3>Categories</h3>
                    <div id='picture'></div>
                    <h4>{ this.props.game.details.categories }</h4>
                  </div>
                </div>
                <div id='right'>
                  <div className='post' id='community'>
                    <div id='picture'></div>
                    <h3>Community</h3>
                    <h4>{ this.props.game.details.community }</h4>
                  </div>
                  <div className='post' id='playing_time'>
                    <div id='picture'></div>
                    <h3>Playing time</h3>
                    <h4>{ this.props.game.details.time }</h4>
                  </div>
                  <div className='post' id='age'>
                    <div id='picture'></div>
                    <h3>Age</h3>
                    <h4>{ this.props.game.details.age }</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(Game));
