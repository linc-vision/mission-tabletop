import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

import $ from 'jquery.transit';



function mapStateToProps (state) {
  return {
    user: state.user,
    game: state.game
  }
}



class Checkout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedParts: [],
      amount: this.props.game.amount,
      window: 'list',
      thrown: false,
      luck: 0,
      luckMessage: ''
    }
    this.select = this.select.bind(this)
  }

  componentDidMount() {
    this.props.onChangePage(this.props.type)
    if ("luck" in this.props.user.profile) {
      this.setState({
        amount: this.props.user.profile.luck.id === this.props.game.id ? (this.props.game.amount - ((this.props.game.amount) * (this.props.user.profile.luck.discount / 100))).toFixed(2) : this.props.game.amount
      })
    }
  }



  select(id) {
    var initialParts = this.state.selectedParts
    var exist = initialParts.indexOf(id, 1), have;
    if (exist === -1) {
      exist = 0
    }
    for (var i = 0; i < initialParts.length; i++) {
      if (initialParts[i] === id) {
        initialParts.splice(exist, 1)
        have = true
      }
    }
    if (!have) {
      initialParts.push(id)
    }
    this.setState({
      selectedParts: initialParts
    }, () => {
      $('#Checkout #list .item:not(:first-child)').removeClass('active')
      var amountArr = [], amount = 0;
      for (var i = 0; i < this.state.selectedParts.length; i++) {
        $('#Checkout #list .item:not(:first-child)').eq(this.state.selectedParts[i] - 1).addClass('active')
        amountArr.push(this.props.game.parts[this.state.selectedParts[i] - 1].amount)
      }
      for (var a = 0; a < amountArr.length; a++) {
        amount = amount + amountArr[a]
      }
      if ("luck" in this.props.user.profile) {
        if (this.props.user.profile.luck.id === this.props.game.id) {
          this.setState({
            amount: ((this.props.game.amount + amount) - ((this.props.game.amount + amount) * (this.props.user.profile.luck.discount / 100))).toFixed(2)
          })
        }
      } else {
        this.setState({
          amount: (this.props.game.amount + amount).toFixed(2)
        })
      }
    })
  }

  openRollTheDice() {
    this.setState({
      window: 'rtd'
    })
    setTimeout(() => {
      $('#Checkout #rtd').css({
        visibility: "visible"
      })
    }, 1200)
    setTimeout(() => {
      $('#Checkout #content #loading').fadeOut()
    }, 1400)
  }

  rollDice() {
    if (!this.state.thrown) {

      this.setState({
        thrown: true
      })

      var faceRoot = [
        "rotateZ(0)",
        "rotateX(90deg)",
        "rotateY(90deg)",
        "rotateY(-90deg)",
        "rotateX(-90deg)",
        "rotateX(180deg)"
      ]

      var dice = $('#Checkout #rtd #dice');
      var timing = 50;

      $(dice).transition({
        // left: Math.random() * (90 - 10) + 10 + "%",
        // top: Math.random() * (90 - 10) + 10 + "%",
        transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
      }, timing, () => {
        $(dice).transition({
          transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
        }, timing, () => {
          $(dice).transition({
            transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
          }, timing, () => {
            $(dice).transition({
              transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
            }, timing, () => {
              $(dice).transition({
                transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
              }, timing, () => {
                $(dice).transition({
                  transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                }, timing, () => {
                  $(dice).transition({
                    transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                  }, timing, () => {
                    $(dice).transition({
                      transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                    }, timing, () => {
                      $(dice).transition({
                        transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                      }, timing, () => {
                        $(dice).transition({
                          transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                        }, timing, () => {
                          $(dice).transition({
                            transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                          }, timing, () => {
                            $(dice).transition({
                              transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                            }, timing, () => {
                              $(dice).transition({
                                transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                              }, timing, () => {
                                $(dice).transition({
                                  transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                                }, timing, () => {
                                  $(dice).transition({
                                    transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                                  }, timing, () => {
                                    $(dice).transition({
                                      transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                                    }, timing, () => {
                                      $(dice).transition({
                                        transform: "rotateX(" + Math.random() * 360 +  "deg) rotateY(" + Math.random() * 360 +  "deg) rotateZ(" + Math.random() * 360 +  "deg)"
                                      }, timing, () => {
                                        Meteor.call('getLuck', (error, res) => {
                                          if (error) {
                                            alert('Something went wrong. Try again later.')
                                          } else {
                                            var luck = res;
                                            setTimeout(() => {
                                              $(dice).css({
                                                transform: faceRoot[luck - 1]
                                              })
                                            }, 100)
                                            this.getLuck(luck)
                                          }
                                        })
                                      })
                                    })
                                  })
                                })
                              })
                            })
                          })
                        })
                      })
                    })
                  })
                })
              })
            })
          })
        })
      })

    }
  }

  getLuck(number) {

    var discount, message, amount;

    if (number === 1) {
      discount = 20
    } else if (number === 2) {
      discount = 25
    } else if (number === 3) {
      discount = 30
    } else if (number === 4) {
      discount = 35
    } else if (number === 5) {
      discount = 40
    } else if (number === 6) {
      discount = 40
    }

    if (number <= 2) {
      message = 'Nice!'
    } else if (number <= 4) {
      message = 'Very good!'
    } else if (number <= 6) {
      message = 'Wow! Luck on your side!'
    }

    amount = this.state.amount - (this.state.amount * discount) / 100

    this.setState({
      luck: discount,
      luckMessage: message
    })

    setTimeout(() => {
      this.setState({
        amount: amount.toFixed(2)
      })
      $('#Checkout #rtd h1#discount').transition({
        bottom: '5%',
        opacity: 1
      }, 400)
      $('#Checkout #rtd #dice, #Checkout #rtd #dice *').transition({
        top: '45%'
      }, 300)
    }, 1000)

  }

  buy() {
    var data = this.state.selectedParts
    Meteor.call('checkout', data, (error, res) => {
      if (error) {
        console.log(error)
      } else {
        if (res === 'ERROR') {
          alert('Something went wrong. Try again later')
        } else {
          // window.open(res, '_blank');
          window.location.href = res
          // console.log(res)
        }
      }
    })
  }



  render() {
    return (
      <div id='Checkout'>
        <div id='layout'>
          <div id='header'>
            <Link to='/'><div id='close' onClick={ this.props.close }></div></Link>
            <h1>Checkout</h1>
          </div>
          {(() => {
            if (!this.props.user.profile.member) {
              return <div id='notmember'>
                <h2>You are not a Member</h2>
                <p>To make purchases, you need to become a member of <strong>Mission Tabletop</strong> club</p>
                <Link to='/profile/membership'><button>Become a Member</button></Link>
              </div>
            } else {
              if (this.state.window === 'list') {
                return <div id='content'>
                  <div id='layout'>
                    <div id='header'>
                      <h1>Assemble The Set</h1>
                    </div>
                    <div id='content'>
                      <div id='list'>
                        <div className='item active' id='maingame'>
                          <div id='left'>
                            <img src={ this.props.game.icon } alt={ this.props.game.title } />
                            <div id='info'>
                              <p id='name'>{ this.props.game.title }</p>
                            </div>
                          </div>
                          <div id='right'>
                            <h2>{ "$" + this.props.game.amount }</h2>
                          </div>
                        </div>
                        { this.props.game.parts.map((item) => {
                          return <div key={ item.id } className='item' onClick={ this.select.bind(this, item.id) }>
                            <div id='left'>
                              <img src={ item.icon } alt={ item.title } />
                              <div id='info'>
                                <p id='name'>{ item.title }</p>
                              </div>
                            </div>
                            <div id='right'>
                              <h2>{ "$" + item.amount }</h2>
                            </div>
                          </div>
                          })
                        }
                      </div>
                    </div>
                    <div id='footer'>
                      <h1>{ "$" + this.state.amount }</h1>
                    </div>
                  </div>
                </div>
              } else if (this.state.window === 'rtd') {
                return <div id='content'>
                  <div id='layout'>
                    <div id='header'>
                      <h1>Roll The Dice!</h1>
                    </div>
                    <div id='content'>
                      <div id='rtd' onClick={ this.rollDice.bind(this) }>
                        <h1 id='discount'>{ this.state.luckMessage }<br/>{ " You received a discount of -" + this.state.luck + '%' }</h1>
                        <div id='dice'>
                          <div className='dice-face'></div>
                          <div className='dice-face'></div>
                          <div className='dice-face'></div>
                          <div className='dice-face'></div>
                          <div className='dice-face'></div>
                          <div className='dice-face'></div>
                        </div>
                      </div>
                    </div>
                    <div id='footer'>
                      <h1>{ "$" + this.state.amount }</h1>
                    </div>
                  </div>
                  <div id='loading'>
                    <div id='logo'>
                      <div id='icon'></div>
                    </div>
                  </div>
                </div>
              }
            }
          })()}
          {(() => {
            if (this.props.user.profile.member) {
              if (this.state.window === 'rtd') {
                if (this.state.thrown) {
                  return <div id='footer'>
                    <button id='buy' onClick={ this.buy.bind(this) }>Buy</button>
                  </div>
                } else {
                  return <div id='footer'>
                    <button id='rtd' onClick={ this.rollDice.bind(this) }>Roll the Dice!</button>
                  </div>
                }
              } else {
                if ("luck" in Meteor.user().profile) {
                  if (Meteor.user().profile.luck.id === this.props.game.id) {
                    return <div id='footer'>
                      <button id='buy' onClick={ this.buy.bind(this) }>Buy</button>
                    </div>
                  } else {
                    return <div id='footer'>
                      <button id='rtd' onClick={ this.openRollTheDice.bind(this) }>Roll the Dice!</button>
                    </div>
                  }
                } else {
                  return <div id='footer'>
                    <button id='rtd' onClick={ this.openRollTheDice.bind(this) }>Roll the Dice!</button>
                  </div>
                }
              }
            }
          })()}
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(Checkout));
