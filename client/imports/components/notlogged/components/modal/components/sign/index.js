import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import $ from 'jquery.transit';



function mapStateToProps (state) {
  return {
    app: state.app
  }
}

function mapDispatchToProps (dispatch, props) {
  return {
    onUpdateUser: (data) => {
      dispatch({ type: 'UPDATE_USER', payload: data })
    },
    onGetUser: (user) => {
      dispatch({ type: 'GET_USER', payload: user })
    }
  }
}



class Sign extends Component {
  constructor(props) {
    super(props)
    this.state = {
      type: this.props.type,
      name: '',
      nameValid: this.props.type === 'signup' ? false : true,
      email: '',
      emailValid: false,
      password: '',
      passwordValid: false,
      confirmPassword: '',
      confirmPasswordValid: this.props.type === 'signup' ? false : true,
      error: 'Error'
    }
    this.enter = this.enter.bind(this)
    this.sign = this.sign.bind(this)
    this.googleSign = this.googleSign.bind(this)
    this.facebookSign = this.facebookSign.bind(this)
  }

  componentDidMount() {
    if (this.state.type === 'signup' || this.state.type === 'login') {
      document.addEventListener('keydown', this.enter);
      setTimeout(() => {
        $('#App #Modal #Sign input').eq(0).focus()
      }, 1200)
    }
    this.props.onChangePage(this.props.type)
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      type: newProps.type,
      usernameValid: newProps.type === 'signup' ? false : true,
    })
  }



  typeName(e) {
    var target = e.target;
    this.setState({
      name: target.value.replace(/[^a-zA-Z ]/, "")
    }, () => {
      if (this.state.name === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validName(e) {
    var valid = this.state.name.length > 6
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        nameValid: false
      })
      this.error('show', 'Name must contain more than 5 characters')
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        nameValid: true
      })
      this.error('hide')
    }
  }

  typeEmail(e) {
    var target = e.target;
    this.setState({
      email: target.value.toLowerCase()
    }, () => {
      if (this.state.email === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validEmail(e) {
    var validTest = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    var valid = validTest.test(this.state.email)
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        emailValid: false
      })
      this.error('show', 'Please, enter valid email')
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        emailValid: true
      })
      this.error('hide')
    }
  }

  typePassword(e) {
    var target = e.target
    this.setState({
      password: target.value.replace(/[^0-9a-zA-Z_-]/, "")
    }, () => {
      if (this.state.password === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  validPassword(e) {
    var valid = this.state.password.length > 7
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        passwordValid: false
      })
      this.error('show', 'Name must contain more than 6 characters')
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        passwordValid: true
      })
      this.error('hide')
    }
  }

  typeConfirmPassword(e) {
    var target = e.target
    this.setState({
      confirmPassword: target.value
    }, () => {
      if (this.state.confirmPassword === '') {
        $(target).addClass('empty')
      } else {
        $(target).removeClass('empty')
      }
    })
  }

  confirmPasswordValid(e) {
    var valid = this.state.confirmPassword === this.state.password;
    if (!valid) {
      $(e.target).addClass('invalid')
      this.setState({
        confirmPasswordValid: false
      })
      this.error('show', 'Passwords must match')
    } else {
      $(e.target).removeClass('invalid')
      this.setState({
        confirmPasswordValid: true
      })
      this.error('hide')
    }
  }

  enter(e) {
    if (e.which === 13) {
      this.sign()
    }
  }

  sign() {
    if (!this.state.nameValid || !this.state.emailValid || !this.state.passwordValid || !this.state.confirmPasswordValid) {
      this.error('show', "Name, Email or Password isn't valid")
    } else {
      if (this.state.type === "signup") {
        var userData = {
          name: this.state.name,
          email: this.state.email,
          password: this.state.password
        }
        Meteor.call('createNewUser', userData, (error, result) => {
          if (error) {
            this.error('show', error.reason)
          } else {
            Meteor.loginWithPassword(userData.email, userData.password, (error, result) => {
              if (error) {
                this.error('show', error.reason)
              } else {
                this.props.onGetUser(Meteor.user());
                this.props.onUpdateUser();
                this.success()
              }
            })
          }
        })
      } else if (this.state.type === "login") {
        Meteor.loginWithPassword(this.state.email, this.state.password, (error, result) => {
          if (error) {
            if (error.reason === "User not found") {
            this.error('show', "The account does not exist. Please double check the email entered.")
            } else {
              this.error('show', error.reason)
            }
          } else {
            this.props.onGetUser(Meteor.user());
            this.props.onUpdateUser();
            this.success()
          }
        })
      }
    }
  }

  googleSign() {
    Meteor.loginWithGoogle({ requestPermissions: ['profile', 'email'] }, (error, result) => {
      if (error) {
        this.error('show', error.reason)
      } else {
        this.props.onGetUser(Meteor.user());
        this.props.onUpdateUser();
        this.success()
      }
    })
  }

  facebookSign() {
    Meteor.loginWithFacebook({ requestPermissions: ['email'] }, (error, result) => {
      if (error) {
        this.error('show', error.reason)
      } else {
        this.props.onGetUser(Meteor.user());
        this.props.onUpdateUser();
        this.success()
      }
    })
  }

  success() {
    $('#close').click()
    $('#App').scrollTop(0)
  }

  error(state, message = 'Error') {
    if (state === 'show') {
      this.setState({
        error: message
      }, () => {
        $('#Sign #error').fadeIn(200).transition({
          opacity: 1,
          bottom: 0
        }, 300)
      })
    } else if (state === 'hide') {
      $('#Sign #error').transition({
        opacity: 0,
        bottom: '-8px'
      }, 300).fadeIn(200, () => {
        this.setState({
          error: 'Error'
        })
      })
    }
  }



  render() {
    return (
      <div id='Sign'>
        <div id='layout'>
          <div id='header'>
            <Link to='/'><div id='close' onClick={ this.props.close }></div></Link>
            <h1>{ this.state.type === "signup" ? "Sign up" : "Login" }</h1>
            <div id='or'>
              <h6>or</h6>
              <Link to={ this.state.type === "login" ? "/signup" : "/login" }><h5>{ this.state.type === "login" ? "Sign up" : "Login" }</h5></Link>
            </div>
          </div>
          <div id='content'>
            <div id='left'>
              {(() => {
                if (this.state.type === 'signup') {
                  return <div>
                    <input className='empty' type="text" name="name" required onChange={ this.typeName.bind(this) } value={ this.state.name } onBlur={ this.validName.bind(this) }/>
                    <span>Your Name</span>
                  </div>
                }
              })()}
              <div>
                <input className='empty' type="email" name="email" required onChange={ this.typeEmail.bind(this) } value={ this.state.email } onBlur={ this.validEmail.bind(this) }/>
                <span>Your email</span>
              </div>
              <div>
                <input className='empty' type="password" name="password" required onChange={ this.typePassword.bind(this) } value={ this.state.password } onBlur={ this.validPassword.bind(this) }/>
                <span>Your password</span>
              </div>
              {(() => {
                if (this.state.type === 'signup') {
                  return <div>
                    <input className='empty' type="password" name="confirm" required onChange={ this.typeConfirmPassword.bind(this) } value={ this.state.confirmPassword } onBlur={ this.confirmPasswordValid.bind(this) }/>
                    <span>Confirm password</span>
                  </div>
                }
              })()}
            </div>
            <div id='right' onClick={ this.sign }></div>
          </div>
          <div id='footer'>
            <div className='socials' id='google' onClick={ this.googleSign }>
              <div id='icon'></div>
              <p>{ this.state.type === "signup" ? "Sign up" : "Login" } with Google</p>
            </div>
            <div className='socials' id='facebook' onClick={ this.facebookSign }>
              <div id='icon'></div>
              <p>{ this.state.type === "signup" ? "Sign up" : "Login" } with Facebook</p>
            </div>
            <p id='error'>{ this.state.error }</p>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sign));
