import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import gt from './../../../../../../../img/pictures/game_title.png'
import pic1 from './../../../../../../../img/pictures/pic_1.png'
import pic2 from './../../../../../../../img/pictures/pic_2.png'
import pic3 from './../../../../../../../img/pictures/pic_3.png'



class Pgames extends Component {
  constructor(props) {
    super(props)
    this.state = {
      gameTitle: 'Catan'
    }
  }

  componentDidMount() {
    this.props.onChangePage(this.props.type)
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      gameTitle: newProps.gameTitle
    })
  }



  render() {
    return (
      <div id='Pgames'>
        <div id='layout'>
          <div id='header'>
            <Link to='/'><div id='close' onClick={ this.props.close }></div></Link>
            <h1>Previous Games</h1>
          </div>
          <div id='content'>
            <div id='picture'>
              <img src={ gt } alt="gt" />
            </div>
            <h2>Lorem ipsum dolor sit amet, consectetur</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
            <div id='img'>
              <div className='img'><img src={ pic1 } alt="pic" /></div>
              <div className='img'><img src={ pic2 } alt="pic" /></div>
              <div className='img'><img src={ pic3} alt="pic" /></div>
            </div>
          </div>
          <div id='footer'>
            <p>June, 2018</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Pgames;
