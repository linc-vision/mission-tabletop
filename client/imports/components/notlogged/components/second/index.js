import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import $ from 'jquery.transit';



function mapStateToProps (state) {
  return {
    game: state.game
  }
}



class Second extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secondThingsHeight: 'auto'
    }
  }

  componentDidMount() {
    var secondThingsHeight = [],
        things = $('#App #Second #content #things > .thing');
    things.map((item) => {
      secondThingsHeight.push(things.eq(item).outerHeight())
      return false
    })
    secondThingsHeight.sort((a,b) => { return b-a })
    this.setState({
      secondThingsHeight: secondThingsHeight[0].toFixed(0) + 'px'
    })
  }



  render() {
    return (
      <div id='Second'>
        <div id='layout'>
          <div id='header'>
            <div id='left'>
              <img src='/img/icons/logo.png' alt="Mission Tabletop Logo" />
              <h2>Mission Tabletop</h2>
            </div>
            <div id='right'>
              <Link to='/login'><button id='login' onClick={ this.props.openModal.bind(this, 'login') }>Log in</button></Link>
              <Link to='/signup'><button id='signup' onClick={ this.props.openModal.bind(this, 'signup') }>Sign up</button></Link>
            </div>
          </div>
          <div id='content'>
            <div id='title'>
              <h1>A Gamer’s Odyssey</h1>
              <h2>A New Board Game Every Month. Deeply Discounted</h2>
            </div>
            <div id='things'>
              <div className='thing' id='first' style={{ height: this.state.secondThingsHeight }}>
                <div id='icon'></div>
                <h3>A Curated Buying Experience</h3>
                <p>Mission Tabletop provides a smooth and easy process to buy and learn how to play the latest and greatest board games we hand pick each month. Our mission at Mission Tabletop is to save our members money and make your buying experience hassle-free, so we offer <strong>serious discounts</strong>, with a minimum of 20% all the way to 40% off retail price.</p>
              </div>
              <div className='thing' id='second' style={{ height: this.state.secondThingsHeight }}>
                <div id='icon'></div>
                <h3>Roll The Dice!</h3>
                <p>Your discount is in your hand! Mission Tabletop’s exclusive members have an awesome and unique opportunity: use our Roll-A-Dice ("RAD") System where you roll a dice to find out how much of a discount you get. Whatever the case, you'll always receive at least a 20% deal, with the potential of up to 40%. Roll for discount!</p>
              </div>
              <div className='thing' id='third' style={{ height: this.state.secondThingsHeight }}>
                <div id='icon'></div>
                <h3>Journey Through The Tabletop Fable</h3>
                <p>Just as many board games provide a story for the players, Mission Tabletop sets itself apart from other online board game stores by creating an evolving storyline  - <a href='https://blog.missiontabletop.com/' target="_blank" rel="noopener noreferrer">The Tabletop Fable</a> - for our members. This Fable is our way of connecting with our audience and telling you about our new games, updates, and announcements with every chapter we release. Follow along on this journey!</p>
              </div>
            </div>
            <div id='magical'>
              <p id='text'><i>“By joining our club, you have entered the hub for many magical game experiences, this is why you have been transported to Mission Tabletop; this is your center to access all the games we will hand-pick for you.”</i> - <strong>Derigo, Patron Helper of Mission Tabletop</strong></p>
              <img src='img/pictures/derigo.png' alt="Mission Tabletop - Derigo" id='derigo'/>
            </div>
            <div id='mGame'>
              <div id='title'>
                <h2>The Game of the Month</h2>
              </div>
              <div id='wrapper'>
                <div id='left'><img src={ this.props.game.images.game_icon } alt={ this.props.game.title }/></div>
                <div id='right'><img src='img/pictures/months_game.png' alt="The Board Game of the Month" /></div>
                <div id='content'>
                  <div id='picture'>
                    <img src={ this.props.game.images.game_title } alt={ this.props.game.title } />
                  </div>
                  <h2>{ this.props.game.first.title }</h2>
                  <p>{ this.props.game.subtitle }</p>
                  <Link to='/game'><button onClick={ this.props.openModal.bind(this, 'game') }>Join to get</button></Link>
                </div>
              </div>
              { /*<p className='link' onClick={ this.props.openModal.bind(this, 'pgames') }>See our previous Games</p>*/ }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(Second));
