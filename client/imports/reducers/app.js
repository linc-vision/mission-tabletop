import { Meteor } from 'meteor/meteor';



var initial = {
  user: Meteor.userId() !== null,
  pageTitle: 'Mission Tabletop'
}



export default function (state = initial, action) {
  switch (action.type) {
    case 'CHANGE_PAGE': {

      var pageTitle;

      if (action.payload === 'main') {
        pageTitle = document.title = 'Mission Tabletop'
      } else if (action.payload === 'contact') {
        pageTitle = document.title = 'Contact Us - Mission Tabletop'
      } else if (action.payload === 'profile') {
        pageTitle = document.title = 'Profile - Mission Tabletop'
      } else if (action.payload === 'checkout') {
        pageTitle = document.title = 'Checkout - Mission Tabletop'
      } else if (action.payload === 'game') {
        pageTitle = document.title = 'Game - Mission Tabletop'
      } else if (action.payload === 'pgames') {
        pageTitle = document.title = 'Previous Games - Mission Tabletop'
      } else if (action.payload === 'ppolicy') {
        pageTitle = document.title = 'Privacy Policy - Mission Tabletop'
      } else if (action.payload === 'rpolicy') {
        pageTitle = document.title = 'Return Policy - Mission Tabletop'
      } else if (action.payload === 'signup') {
        pageTitle = document.title = 'Sign Up - Mission Tabletop'
      } else if (action.payload === 'login') {
        pageTitle = document.title = 'Login - Mission Tabletop'
      }

      state.pageTitle = pageTitle
      return state;

    }

    case 'UPDATE_USER': {

      state.user = Meteor.userId() !== null
      return state;

    }

    default: {}

  }
  return state;
}
