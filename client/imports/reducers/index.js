import { combineReducers } from 'redux';

import app from './app';
import user from './user';
import game from './game';



const reducers = combineReducers ({
  app, user, game
})



export default reducers;
