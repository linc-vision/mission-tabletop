import { Mongo } from 'meteor/mongo';
import { withTracker } from 'meteor/react-meteor-data';
import React, { Component } from 'react';
import connectMeteor from 'react-redux-meteor-data';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import $ from 'jquery.transit';

import Notlogged from './components/notlogged/index';
import Logged from './components/logged/index';



const Game = new Mongo.Collection('games');
const gameSubscription = Meteor.subscribe('game');



function mapStateToProps (state) {
  return {
    app: state.app,
    user: state.user
  }
}

function mapTrackerToProps () {
  var game = Game.find({}).fetch();

  return {
    game
  }
}

function mapDispatchToProps (dispatch, props) {
  return {
    onStart: (data) => {
      dispatch({ type: 'START', payload: data })
    },
    onUpdateUser: (data) => {
      dispatch({ type: 'UPDATE_USER', payload: data })
    },
    onGetUser: (user) => {
      dispatch({ type: 'GET_USER', payload: user })
    }
  }
}



class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      started: gameSubscription.ready()
    }
  }

  componentWillMount() {
    if (this.props.location.search !== "") {
      Meteor.call('executeBillingAgreement', this.props.location.search, (error, res) => {
        if (error) {
          console.log(error)
          this.props.history.push('/')
        } else {
          if (res === 'ERROR') {
            this.props.history.push('/')
          } else {
            this.props.history.push('/profile/membership')
            this.props.onGetUser(Meteor.user())
          }
        }
      })
    }
  }

  componentDidMount() {
    if (this.props.location.search === "") {
      Meteor.call('checkBillingAgreement', (error, res) => {
        if (error) {
          console.log(error);
        } else {
          if (res === 'SUCCESS') {
            this.props.onGetUser(Meteor.user())
          }
        }
      })
    }
  }

  componentWillReceiveProps(newProps) {
    this.props.onUpdateUser()
    if (!this.state.started) {
      if (gameSubscription.ready()) {
        if (this.props.app.user) {
          this.props.onGetUser(Meteor.user())
        }
        this.props.onStart(newProps.game[0]);
        this.setState({
          started: true
        }, () => {
          this.load()
          if (Meteor.userId() !== null) {
            Meteor.call('checkCheckout', (error, res) => {
              if (error) {
                console.log(error);
              } else {
                this.props.onGetUser(Meteor.user())
              }
            })
          }
        })
      }
    }
  }

  load() {
    $('#loading > #logo').fadeOut(100, () => {
      $('#loading').fadeOut()
    })
  }



  render() {
    return (
      <div id='App'>
        <div id='layout'>
          {(() => {
            if (this.state.started) {
              return <Switch>
                {(() => {
                  if (this.props.app.user) {
                    return <Route path='/' component={ Logged }/>
                  } else {
                    return <Route path='/' component={ Notlogged }/>
                  }
                })()}
                <Redirect path='*' to='/'/>
              </Switch>
            }
          })()}
        </div>
        <div id='loading'>
          <div id='logo'>
            <div id='icon'></div>
          </div>
        </div>
        <div id='loadingOver'>
          <div id='logo'>
            <div id='icon'></div>
          </div>
        </div>
      </div>
    );
  }
}



export default withRouter(connectMeteor(mapTrackerToProps, mapStateToProps, mapDispatchToProps)(App));
