import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Accounts } from 'meteor/accounts-base';
import Future from 'fibers/future';
import fetch from 'node-fetch';
// import paypal from 'paypal-rest-sdk';
import Paypal from 'paypal-express-checkout-es6';
import Client from 'shopify-buy';
import states from 'us-state-codes';
import url from 'url';
import { Email } from 'meteor/email';



const Games = new Mongo.Collection('games');



Meteor.startup(() => {

  Meteor.publish('game', () => {
    return Games.find({id: 1});
  })



  Accounts.loginServiceConfiguration.remove({
    service: "facebook"
  });

  Accounts.loginServiceConfiguration.insert({
    service: "facebook",
    appId: "1080178188812894",
    secret: "7297f35e7e471bc2f185ceae06ac9786"
  });

  Accounts.loginServiceConfiguration.remove({
    service: "google"
  });

  Accounts.loginServiceConfiguration.insert({
    service: "google",
    clientId: "176152484602-ck46n5clce7fej7mhq8s7il89auhf94c.apps.googleusercontent.com",
    secret: "VLQIM1jwMeJVcLFsyL15fGjG"
  });

  Accounts.onCreateUser((options, user) => {
    if (!('profile' in options)) { options.profile = {} }

    var data

    var SendWelcomeEmail = (email) => {
      var smtp = {
        username: 'hexa.vc.contact.us@gmail.com',
        password: 'hexahello',
        server:   'smtp.gmail.com',
        port: 587
      }

      process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;

      var options = {
        from: 'Mission Tabletop <hexa.vc.contact.us@gmail.com>',
        to: email,
        subject: 'Welcome to Mission Tabletop Club!',
        html: "<html><body><style>html {background-color: #162136; padding: 3em} body {text-align: center; background-color: #162136;} img {width: 52px; margin: 2em 0} h2 { font-family: 'Arial Black', 'Arial'; font-weight: bold;} * { font-family: 'Roboto', 'Candara', 'Arial'; letter-spacing: 1px; color: #FFF; }</style><img src='https://firebasestorage.googleapis.com/v0/b/mission-tabletop.appspot.com/o/logo.png?alt=media&token=059c885d-aed3-430a-8457-347975746634' alt='Mission Tabletop Logo' /><h2>Welcome!</h2><p>Thank you for signing up!<br/>To buy games in our club, you must become a member: </p><a href='https://missiontabletop.com/profile/membership'>https://missiontabletop.com/profile/membership</a><br/><br/><br/></body></html>"
      }

      Email.send(options)
    }

    if ('google' in user.services) {
      data = {
        service: 'google',
        member: false,
        name: user.services.google.name,
        email: user.services.google.email,
        purchase_history: [],
        billing_address: {
          name: user.services.google.name,
          phone: "",
          address: "",
          country: "USA",
          state: "",
          city: "",
          zipcode: ""
        }
      }
      SendWelcomeEmail(user.services.google.email)
    } else if ('facebook' in user.services) {
      data = {
        service: 'facebook',
        member: false,
        name: user.services.facebook.name,
        email: user.services.facebook.email,
        purchase_history: [],
        billing_address: {
          name: user.services.facebook.name,
          phone: "",
          address: "",
          country: "USA",
          state: "",
          city: "",
          zipcode: ""
        }
      }
      if ("email" in user.services.facebook) {
        SendWelcomeEmail(user.services.facebook.email)
      }
    } else if ('password' in user.services) {
      data = {
        service: null,
        member: false,
        name: options.profile.name,
        email: options.email,
        purchase_history: [],
        billing_address: {
          name: options.profile.name,
          phone: "",
          address: "",
          country: "USA",
          state: "",
          city: "",
          zipcode: ""
        }
      }
      SendWelcomeEmail(options.email)
    }

    user.profile = data

    return user
  })



  // var paypalConfig, paypalMode = 'live';
  //
  // if (paypalMode === 'test') {
  //   paypalConfig = {
  //     'mode': 'sandbox',
  //     'client_id': 'AZ-4bsT4yyrl-5-1X3r0D7AmvEI5l9jplli7s3hhIjeMm9ZDIStwuL15qrJt98QGtLC3lAzcysEi45cN',
  //     'client_secret': 'EEWsMTBEawsZSI-YXn-RbJ5OyamNJSbPeYwq9-M-z2_vZ8ocNE1HvKefm39b-gMQgeV_XAERTDkw6sY_'
  //   }
  // } else if (paypalMode === 'live') {
  //   paypalConfig = {
  //     'mode': 'live',
  //     'client_id': 'AUCymux0emL4Dzs9e-N4EOJQY3l3GzCBjGLXgzMP84BKSr7ldw44y9McErHAtm8DRYUfQj3UyVIuYbrr',
  //     'client_secret': 'EEXiMckGZzmh_z1xQ1EazBlM8MFwNt_sCWpM49tsXZEPcqaYCDISNKnC3QBFxXp7LZ0xLctGvH0FfAUj'
  //   }
  // }
  //
  // paypal.configure(paypalConfig)

  var paypalConfig, operationUrl, returnUrl, cancelUrl, paypalMode = 'live';

  if (paypalMode === 'test') {
    paypalConfig = {
      USER: 'yc-facilitator_api1.hexa.systems',
      PWD: 'H74LPW96HPX5QMTR',
      SIGNATURE: 'AcKaLg3oWXNXWF.wnsKqR3qpzAE4AGq8anuveZo3pZ5XkzxIsu2PqRIr',
      isSandbox: true
    }
    operationUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='
    returnUrl = cancelUrl = 'http://localhost:3000'
  } else if (paypalMode === 'live') {
    paypalConfig = {
      USER: 'yc_api2.hexa.systems',
      PWD: 'S6ZGHK7X54Y8GTEG',
      SIGNATURE: 'ADQg1Ac5YeGKRxsfr78S39Xw5ejxAlBclCI98u.tw0aKMsA2AV0XWmcK',
      isSandbox: false
    }
    operationUrl = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='
    returnUrl = cancelUrl = 'https://missiontabletop.com'
  }

  const paypal = new Paypal(paypalConfig)



  const Shopify = Client.buildClient({
    domain: 'mission-tabletop.myshopify.com',
    storefrontAccessToken: '2d9e3ebaa990539332803401be2cf47a'
  }, fetch)



  Meteor.methods({

    createNewUser: (userData) => {
      if (Meteor.userId() === null) {
        var data = {
          username: userData.email,
          email: userData.email,
          password: userData.password,
          profile: {
            name: userData.name,
            email: userData.email
          }
        }
        Accounts.createUser(data)
      } else {
        throw new Meteor.Error('user-already')
      }
    },



    setUserPassword: (newPassword) => {
      Accounts.setPassword(Meteor.userId(), newPassword, { logout: false })
    },



    // updateUserProfile: (userData) => {
    //   if (Meteor.userId() !== null) {
    //     Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.billing_address": userData } })
    //     Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.billing_address.country": 'USA' } })
    //   }
    // },
    //
    //
    //
    //
    //
    //
    // getBillingPlans: () => {
    //   var list_billing_plan = {
    //     'page_size': 5,
    //     'status': 'ACTIVE',
    //     'page': 0,
    //     'total_required': 'yes'
    //   }
    //
    //   paypal.billingPlan.list(list_billing_plan, function (error, res) {
    //     if (error) {
    //       throw error;
    //     } else {
    //       console.log("Billing Plans List:");
    //       console.log(res)
    //     }
    //   })
    // },
    //
    //
    //
    // getBillingPlan: () => {
    //   var billingPlanIDs = {
    //     monthly: "P-4HU85785TS6548443NAS5MZA",
    //     annual: "P-7E7259141H400013YNASHOGY"
    //   }
    //
    //   paypal.billingPlan.get(billingPlanIDs.annual, (error, res) => {
    //     if (error) {
    //       throw error
    //     } else {
    //       console.log(res)
    //     }
    //   })
    // },



    selectBillingPlan: (period) => {
      if (Meteor.userId() !== null && !Meteor.user().profile.member) {

        // var billingAgreementAttributes;
        // var userData = data;
        //
        // var billingPlanAttributes = {
        //   "description": userData.selectedType === "monthly" ? "Mission Tabletop Club Membership Monthly" : "Mission Tabletop Club Membership Annual",
        //   "merchant_preferences": {
        //     "auto_bill_amount": "yes",
        //     "cancel_url": "https://missiontabletop.com",
        //     "initial_fail_amount_action": "CONTINUE",
        //     "max_fail_attempts": "1",
        //     "return_url": "https://missiontabletop.com",
        //     "setup_fee": {
        //       "currency": "USD",
        //       "value": "0"
        //     }
        //   },
        //   "name": userData.selectedType === "monthly" ? "Membership Monthly" : "Membership Annual",
        //   "payment_definitions": [
        //     {
        //       "amount": {
        //         "currency": "USD",
        //         "value": userData.selectedType === "monthly" ? "1" : "10"
        //       },
        //       "charge_models": [
        //         {
        //           "amount": {
        //             "currency": "USD",
        //             "value": "0"
        //           },
        //           "type": "SHIPPING"
        //         },
        //         {
        //           "amount": {
        //             "currency": "USD",
        //             "value": "0"
        //           },
        //           "type": "TAX"
        //         }
        //       ],
        //       "cycles": "0",
        //       "frequency": userData.selectedType === "monthly" ? "MONTH" : "YEAR",
        //       "frequency_interval": "1",
        //       "name": userData.selectedType === "monthly" ? "Membership Monthly" : "Membership Annual",
        //       "type": "REGULAR"
        //     }
        //   ],
        //   "type": "INFINITE"
        // }
        //
        // paypal.billingPlan.create(billingPlanAttributes, Meteor.bindEnvironment((error, billingPlan) => {
        //   if (error) {
        //     console.log("Billing Plan creating ERROR: " + error.response)
        //     future.return('INVALID_DATA')
        //   } else {
        //     var billingPlanId = billingPlan.id;
        //       var billing_plan_update_attributes = [
        //         {
        //           "op": "replace",
        //           "path": "/",
        //           "value": {
        //             "state": "ACTIVE"
        //           }
        //         }
        //       ]
        //
        //       paypal.billingPlan.update(billingPlanId, billing_plan_update_attributes, Meteor.bindEnvironment((error) => {
        //         if (error) {
        //           console.log("Billing Plan updating to active status ERROR: " + error.response)
        //           future.return('INVALID_DATA')
        //         } else {
        //           var isoDate = new Date();
        //           isoDate.setSeconds(isoDate.getSeconds() + 3600);
        //           isoDate.toISOString()
        //
        //           billingAgreementAttributes = {
        //             "name": userData.selectedType  === 'monthly' ? "Mission Tabletop Membership - Monthly" : "Mission Tabletop Membership - Annual",
        //             "description": userData.selectedType  === 'monthly' ?  "Agreement for Monthly Mission Tabletop Membership" : "Agreement for Annual Mission Tabletop Membership",
        //             "start_date": isoDate,
        //             "plan": {
        //               "id": billingPlanId
        //             },
        //             "payer": {
        //               "payment_method": "credit_card",
        //               "funding_instruments": [
        //                 {
        //                   "credit_card": {
        //                     "number": userData.card_data.number,
        //                     "type": userData.card_data.type,
        //                     "expire_month": parseInt(userData.card_data.expMonth, 10),
        //                     "expire_year": parseInt(userData.card_data.expYear, 10) < 2000 ? 2000 + parseInt(userData.card_data.expYear, 10) : parseInt(userData.card_data.expYear, 10),
        //                     "cvv2": userData.card_data.cvv2,
        //                     "first_name": userData.card_data.firstName,
        //                     "last_name": userData.card_data.lastName
        //                   }
        //                 }
        //               ]
        //             }
        //           }
        //
        //           paypal.billingAgreement.create(billingAgreementAttributes, Meteor.bindEnvironment((error, billingAgreement) => {
        //             if (error) {
        //               console.log("Billing Agreement creating ERROR:")
        //               console.log(error.response)
        //               future.return('INVALID_DATA')
        //             } else {
        //               var period = billingAgreement.description === 'Agreement for Monthly Mission Tabletop Membership' ? 'monthly' : 'annual'
        //               Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.member": true } })
        //               Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement": billingAgreement } })
        //               Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement.period": period } })
        //
        //               future.return('SUCCESS')
        //             }
        //           }))
        //         }
        //       }))
        //   }
        // }))

        var future = new Future()

        var data;

        if (period === 'monthly') {
          data = {
            L_BILLINGTYPE0: 'RecurringPayments',
            L_BILLINGAGREEMENTDESCRIPTION0: 'The Mission Tabletop Club Membership - Monthly',
            PAYMENTREQUEST_0_AMT: 1.00,
            L_PAYMENTTYPE0: 'InstantOnly',
            LANDINGPAGE: 'Billing',
            USERSELECTEDFUNDINGSOURCE: 'CreditCard',
            RETURNURL: returnUrl,
            CANCELURL: cancelUrl
          }
        } else if (period === 'annual') {
          data = {
            L_BILLINGTYPE0: 'RecurringPayments',
            L_BILLINGAGREEMENTDESCRIPTION0: 'The Mission Tabletop Club Membership - Annual',
            PAYMENTREQUEST_0_AMT: 10.00,
            L_PAYMENTTYPE0: 'InstantOnly',
            LANDINGPAGE: 'Billing',
            USERSELECTEDFUNDINGSOURCE: 'CreditCard',
            RETURNURL: returnUrl,
            CANCELURL: cancelUrl
          }
        }

        paypal.call('SetExpressCheckout', data).then((res) => {
          Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement.period": period } })
          var result = operationUrl + res.responseBody
          future.return(result)
        })

      }

      return future.wait()
    },


    executeBillingAgreement(token) {
      if (Meteor.userId() !== null && !Meteor.user().profile.member) {
        var future = new Future()

        var token = url.parse(token, true).query.token
        var  RRProfileData;

        if (Meteor.user().profile.membership_agreement.period === 'annual') {
          RRProfileData = {
            DESC: 'The Mission Tabletop Club Membership - Annual',
            BILLINGPERIOD: 'Year',
            AMT: 10.00
          }
        } else if (Meteor.user().profile.membership_agreement.period === 'monthly') {
          RRProfileData = {
            DESC: 'The Mission Tabletop Club Membership - Monthly',
            BILLINGPERIOD: 'Month',
            AMT: 1.00
          }
        }

        paypal.call('GetExpressCheckoutDetails', {
          TOKEN: token
        }).then((res) => {
          if (res.result.BILLINGAGREEMENTACCEPTEDSTATUS === '1') {
            var isoDate = new Date();
                isoDate.setSeconds(isoDate.getSeconds() + 180);
                isoDate.toISOString()
            paypal.call('CreateRecurringPaymentsProfile', {
              TOKEN: token,
              PAYERID: res.result.PAYERID,
              PROFILESTARTDATE: isoDate,
              DESC: RRProfileData.DESC,
              BILLINGPERIOD: RRProfileData.BILLINGPERIOD,
              BILLINGFREQUENCY: 1,
              AMT: RRProfileData.AMT,
              CURRENCYCODE: 'USD',
              COUNTRYCODE: 'US',
              MAXFAILEDPAYMENTS: 0
            }).then((res2) => {
              paypal.call('GetRecurringPaymentsProfileDetails', {
                PROFILEID: res2.result.PROFILEID
              }).then((res3) => {
                var period = res3.result.DESC === 'The Mission Tabletop Club Membership - Annual' ? 'annual' : 'monthly'
                Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.member": true } })
                Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement": res3.result } })
                Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement.period": period } })
                future.return('SUCCESS')
              })
            })
          } else {
            future.return('ERROR')
          }
        })

        return future.wait()
      }
    },



    checkBillingAgreement() {
      if (Meteor.userId() !== null && Meteor.user().profile.member) {
        var future = new Future()

        // paypal.billingAgreement.get(Meteor.user().profile.membership_agreement.id, Meteor.bindEnvironment((error, billingAgreement) => {
        //   if (error) {
        //     future.return('ERROR')
        //   } else {
        //     if (billingAgreement.state === 'Active') {
        //       var period = billingAgreement.description === 'Agreement for Monthly Mission Tabletop Membership' ? 'monthly' : 'annual'
        //       Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement": billingAgreement } })
        //       Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement.period": period } })
        //     } else {
        //       Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.member": false } })
        //       Meteor.users.update({ _id: Meteor.userId() }, { $unset: { "profile.membership_agreement": "" } })
        //     }
        //     future.return('SUCCESS')
        //   }
        // }))

        paypal.call('GetRecurringPaymentsProfileDetails', {
          PROFILEID: Meteor.user().profile.membership_agreement.PROFILEID
        }).then((res) => {
          if (res.result.STATUS === 'Active') {
            var period = res.result.DESC === 'The Mission Tabletop Club Membership - Annual' ? 'annual' : 'monthly'
            Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement": res.result } })
            Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.membership_agreement.period": period } })
          } else {
            Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.member": false } })
            Meteor.users.update({ _id: Meteor.userId() }, { $unset: { "profile.membership_agreement": "" } })
          }
          future.return('SUCCESS')
        })

        return future.wait()
      }
    },



    cancelMembership() {
      if (Meteor.userId() !== null && Meteor.user().profile.member) {
        // var dateValid = new Date(Meteor.user().profile.membership_agreement.agreement_details.next_billing_date) > new Date()
        // if (Meteor.userId() !== null && Meteor.user().profile.member && dateValid) {
        //   var future = new Future()
        //   var billingAgreementId = Meteor.user().profile.membership_agreement.id
        //   var suspend_note = { "note": "Suspending the agreement" }
        //   var cancel_note = { "note": "Canceling the agreement" }
        //
        //   paypal.billingAgreement.suspend(billingAgreementId, cancel_note, Meteor.bindEnvironment((error, res) => {
        //     if (error) {
        //       future.return('ERROR')
        //     } else {
        //       paypal.billingAgreement.cancel(billingAgreementId, cancel_note, Meteor.bindEnvironment((error, res) => {
        //         if (error) {
        //           future.return('ERROR')
        //         } else {
        //           Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.member": false } })
        //           Meteor.users.update({ _id: Meteor.userId() }, { $unset: { "profile.membership_agreement": "" } })
        //           future.return('SUCCESS')
        //         }
        //       }))
        //     }
        //   }))
        //
        //   return future.wait()
        //
        // } else {
        //
        //   return 'ERROR'
        //
        // }

        var dateValid = new Date(Meteor.user().profile.membership_agreement.NEXTBILLINGDATE) > new Date()

        // if (Meteor.userId() !== null && Meteor.user().profile.member && dateValid) {
        if (Meteor.userId() !== null && Meteor.user().profile.member) {

          var future = new Future()

          paypal.call('ManageRecurringPaymentsProfileStatus', {
            PROFILEID: Meteor.user().profile.membership_agreement.PROFILEID,
            ACTION: 'Cancel'
          }).then((res) => {
            Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.member": false } })
            Meteor.users.update({ _id: Meteor.userId() }, { $unset: { "profile.membership_agreement": "" } })
            future.return('SUCCESS')
          })

          return future.wait()

        } else {

          return 'ERROR'

        }

      }
    },



    checkout(data) {
      if (Meteor.userId() !== null && Meteor.user().profile.member) {
        var future = new Future()
        var selected = data;
            selected.unshift(0)
        var selectedProducts = [];

        Shopify.collection.fetchWithProducts("Z2lkOi8vc2hvcGlmeS9Db2xsZWN0aW9uLzc0MzA1NDM3ODE0").then((collection) => {
          selected.map((item) => {
            selectedProducts.push({
              variantId: collection.products.reverse()[item].variants.find((el) => { return el.title === (Meteor.user().profile.luck.discount + '%') }).id,
              quantity: 1
            })
          })

          // var userData = Meteor.user().profile;
          var checkoutInput = {
            // email: userData.email,
            // shippingAddress: {
            //   address1: userData.billing_address.address,
            //   address2: "",
            //   city: userData.billing_address.city,
            //   company: "",
            //   country: "US",
            //   firstName: userData.billing_address.name.split(' ')[0],
            //   lastName: userData.billing_address.name.split(' ')[1],
            //   phone: userData.billing_address.phone,
            //   province: states.getStateCodeByStateName((states.sanitizeStateName(userData.billing_address.state))),
            //   zip: userData.billing_address.zipcode
            // },
            lineItems: selectedProducts
          }

          Shopify.checkout.create(checkoutInput).then((checkout) => {
            Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.checkout": checkout.id } })
            future.return(checkout.webUrl)
          })

        })

        return future.wait()
      }
    },



    checkCheckout() {
      if (Meteor.userId !== null && Meteor.user().profile.member) {
        if ("checkout" in Meteor.user().profile) {
          var future = new Future()
          var game = Games.find({ id: 1 }).fetch()

          Shopify.checkout.fetch(Meteor.user().profile.checkout).then((checkout) => {
            if (checkout.completedAt !== null) {
              var data = Meteor.user().profile.purchase_history
              data.push({
                id: data.length,
                name: game[0].title,
                icon: game[0].icon,
                date: new Date(),
                amount: checkout.totalPrice
              })
              Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.purchase_history": data } })
              Meteor.users.update({ _id: Meteor.userId() }, { $unset: { "profile.checkout": "" } })
              Meteor.users.update({ _id: Meteor.userId() }, { $unset: { "profile.luck": "" } })

              future.return("SUCCESS")
            } else {
              Meteor.users.update({ _id: Meteor.userId() }, { $unset: { "profile.checkout": "" } })

              future.return("SUCCESS")
            }
          })

          return future.wait()
        }
      }
    },



    getLuck() {
      if (Meteor.userId() !== null && Meteor.user().profile.member) {
        var number = Math.round(Math.random() * 5) + 1
        var discount;
        if (number === 1) {
          discount = 20
        } else if (number === 2) {
          discount = 25
        } else if (number === 3) {
          discount = 30
        } else if (number === 4) {
          discount = 35
        } else if (number === 5) {
          discount = 40
        } else if (number === 6) {
          discount = 40
        }
        if ("luck" in Meteor.user().profile) {
          if (Meteor.user().profile.luck.id !== 1) {
            var future = new Future()
            Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.luck": { id: 1, discount: discount } } })
            future.return(number)
          } else {
            var future = new Future()
            Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.luck": { id: 1, discount: discount } } })
            future.return(number)
          }
        } else {
          var future = new Future()
          Meteor.users.update({ _id: Meteor.userId() }, { $set: { "profile.luck": { id: 1, discount: discount } } })
          future.return(number)
        }

        return future.wait()
      }
    },



    contactUs(data) {
      if (Meteor.userId() !== null) {
        var future = new Future()

        var smtp = {
          username: 'hexa.vc.contact.us@gmail.com',
          password: 'hexahello',
          server:   'smtp.gmail.com',
          port: 587
        }

        process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;

        var options = {
          from: 'Mission Tabletop Contact Form <hexa.vc.contact.us@gmail.com>',
          to: 'hello@hexa.systems',
          subject: 'Hello!' + ' | Mission Tabletop',
          html: "<html><body><h2>" + data.name + "<h2/><h6>" + data.email + "<h6/><p>" + data.message + "<p/></body></html>"
        }

        Email.send(options)
        future.return('SUCCESS')

        return future.wait()
      }
    }

  })

  Meteor.call('checkBillingAgreement')

})
