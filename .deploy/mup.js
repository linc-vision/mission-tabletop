module.exports = {
  servers: {
    one: {
      host: '13.59.167.193',
      username: 'ubuntu',
      pem: '../../keys/missiongamers.pem'
    }
  },

  app: {
    name: 'mission-tabletop',
    path: '../',

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      ROOT_URL: 'https://missiontabletop.com',
      MONGO_URL: 'mongodb://mongodb/meteor',
      MONGO_OPLOG_URL: 'mongodb://mongodb/local',
    },

    docker: {
      image: 'zodern/meteor:root'
    },

    enableUploadProgressBar: true
  },

  mongo: {
    version: '3.4.1',
    servers: {
      one: {}
    }
  },

  proxy: {
    domains: 'missiontabletop.com,www.missiontabletop.com',

    ssl: {
      crt: '../../keys/missiontabletop.com_ssl_certificate.cer',
      key: '../../keys/_.missiontabletop.com_private_key.key',
      forceSSL: true
    }
  }
};



// module.exports = {
//   servers: {
//     one: {
//       // TODO: set host address, username, and authentication method
//       host: '52.15.229.127',
//       username: 'ubuntu',
//       pem: '../../keys/testmissiontabletop.pem'
//       // password: 'server-password'
//       // or neither for authenticate from ssh-agent
//     }
//   },
//
//   app: {
//     // TODO: change app name and path
//     name: 'mission-tabletop-test',
//     path: '../',
//
//     servers: {
//       one: {},
//     },
//
//     buildOptions: {
//       serverOnly: true,
//     },
//
//     env: {
//       // TODO: Change to your app's url
//       // If you are using ssl, it needs to start with https://
//       ROOT_URL: 'http://test.missiontabletop.com',
//       MONGO_URL: 'mongodb://mongodb/meteor',
//       MONGO_OPLOG_URL: 'mongodb://mongodb/local',
//     },
//
//     docker: {
//       // change to 'abernix/meteord:base' if your app is using Meteor 1.4 - 1.5
//       image: 'abernix/meteord:node-8.9.4-base',
//     },
//
//     // Show progress bar while uploading bundle to server
//     // You might need to disable it on CI servers
//     enableUploadProgressBar: true
//   },
//
//   mongo: {
//     version: '3.4.1',
//     servers: {
//       one: {}
//     }
//   },
//
//   // (Optional)
//   // Use the proxy to setup ssl or to route requests to the correct
//   // app when there are several apps
//
//   // proxy: {
//   //   domains: 'mywebsite.com,www.mywebsite.com',
//
//   //   ssl: {
//   //     // Enable Let's Encrypt
//   //     letsEncryptEmail: 'email@domain.com'
//   //   }
//   // }
// };
